# CERNBox Documentation

## Local deployment

1) Install pip: `pip install --upgrade pip`

2) Install mcdocs package: `pip install mkdocs`

3) Install the theme for mcdocs: `pip install mkdocs-material`

4) If necessarry, install missing extensions and plugins:
`pip install mkdocs-awesome-pages-plugin mkdocs-static-i18n`

5) Run `mkdocs build` and `mkdocs serve`

## Managing navigation

We use [awesome-pages plugin](https://github.com/lukasgeiter/mkdocs-awesome-pages-plugin) for ordering of the navigation menus.

Create a file named .pages in a directory and use the nav attribute to customize the navigation on that level. List the files and subdirectories in the order that they should appear in the navigation

## Translating into French

We use [i18n plugin](https://github.com/ultrabug/mkdocs-static-i18n) with suffix based docs structure. To make a translation: for each _file_.md create a corresponding _file_.fr.md in the same directory. If a file has no corresponding translation, the file in default language will be shown. 

Don't forget to insert the translated file into .pages of the corresponding directory.
