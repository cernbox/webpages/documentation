# Accés par EOS et XRootD

Le point d'entrée XRootD/EOS  (instance serveur) pour vos fichiers CERNbox est `root://eosuser.cern.ch`
et votre répertoire est  `/eos/user/<initial>/<userid>`,  (e.g. `/eos/user/l/laman`)

- Pour créer un répertoire dans votre espace CERNBox:

`% eos mkdir /eos/user/<initial>/<userid>/<my_directory>`, (e.g. `eos mkdir /eos/user/l/laman/office_plans`)

!!! warning "Note"

     Si vous utilisez EOS pour CERNBox et pour votre expérience de physique, vérifier toujours quelle est l'instance EOS à laquelle vous accèdez avant d'exécuter des commandes EOS. 

     Pour être certain d'accèder à la bonne instance EOS, il est recommandé d'utiliser la commande   `% eos <instance server> <command>` avant toute autre commande EOS
     Exemples : 
       - pour avoir la liste des fichiers de l'utilisateur laman dans CERNBox, `>eos root://eosuser.cern.ch ls /eos/user/l/laman`
       - pour interroger l'instance EOSCMS, `>eos root://eoscms.cern.ch <command>`
