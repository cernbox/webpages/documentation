# Windows: Mapped network drive (CIFS/SMB) access

From a Windows or MacOS machine, you may want to access your CERNBox directly from the File Explorer, using the standard mapped network drives and the CIFS/SMB protocol. A cluster of (SMB) gateways is provided for this purpose.

Note that this method only works within the CERN Local Area Network. If you need external online access, you may use the web browser or the [WebDAV](webdav.md) method.

!!! warning "Important"

    When browsing CERNBox resources in such way, please note that Windows shows that "Everyone has full control" if you look in the security settings of those folders. This is **NOT VALID**: permissions are properly enforced by the system, according both to projects' e-groups (where relevant) and to the sharing that you see in the CERNBox web interface!


- Connect to the gateways with `Start` --> `Run` --> `\\cernbox-smb\eos\user` or open the drive `H:` if configured in your system

![SAMBA connexion](../../assets/images/samba_1.png)

You can also type the full path to your CERNBox, for example `\\cernbox-smb\eos\user\d\duret`

- OR, you can change your Windows configuration by mapping your CERNBox to a Network drive, for example W:

![SAMBA mount as network drive](../../assets/images/samba-1.png)

Your files/folders are seen in the drive that you selected (in our example, it was mapped to drive W):

![SAMBA mount as network drive](../../assets/images/samba-2.png)

Remember to disconnect when you have finished:

![SAMBA mount as network drive](../../assets/images/samba-3.png)

## Access to a folder that was 'Shared with you'

It is possible to map a folder that was 'Shared with you' using the Authenticated Share method.

- Retrieve the _EOS path_ of the SPECIFIC folder that was in 'Shared with you'. Go to the files that have been [Shared with you](../sync-client/add_a_folder_sync_connection.md#eos-path)

- Using the _EOS path_, map it to another network drive (eg. Drive `T:`) (as described above). NB: remember to replace all `/` with `\`, (eg.`\\cernbox-smb\eos\user\g\gonzalhu\a_shared_folder`).

This method also works for project spaces, where you would map a path such as `\\cernbox-drive\project\p\project_name\folder`.

On the contrary, it is not possible to map a folder that was shared via anonymous link. In that case, you can only use the web interface.
