# WebDAV

An alternative to access your CERNBox files/folders is to mount CERNBox using WebDAV. This access method also works from outside the CERN Network. Use the following URL:

`https://cernbox.cern.ch/cernbox/webdav/home`

Full access the to underlying storage is available using the full path. For example, something like:
    
For user space: `https://cernbox.cern.ch/cernbox/webdav/eos/user/d/doe` 
For project space: `https://cernbox.cern.ch/cernbox/webdav/eos/project/c/cernbox`  

Note: please do not use these URLs in the web browser.

!!! warning "Suggestions/Limitations"

     - The MacOSX Finder WebDAV Client is not very efficient, you can expect slowdowns depending on your network connectivity
     - The Windows File Explorer provides a limited support of the WebDAV standard and is effectively not working with CERNBox
     - In both cases, we recommend to use Cyberduck (see below) or other 3rd party tools
