# Cyberduck
You can mount CERNBox via Cyberduck, which is an open source client for FTP and SFTP, WebDAV. It is available for Mac OS X and Windows, licensed under the GPL.

- Download and install Cyberduck from the following link: [https://cyberduck.io/?l=en](https://cyberduck.io/?l=en)
- Launch Cyberduck and type your Username and Password:

![Cyberduck step 1](../../assets/images/cyberduck_1.png)

- Then click on **Open Connection** button:

![Cyberduck Step 2](../../assets/images/cyberduck_2.png)

- The **Open Connection** window will pop up, select **WebDav (Https/SSL)** from the drop down menu:

![Cyberduck Step 3](../../assets/images/cyberduck_3.png)

- Then type *cernbox.cern.ch* in the Server box
- Type your Username and Password
- Click on **More Options** and 
- Insert the path as follows */cernbox/webdav/home*
- Then click **Connect**

![Cyberduck Step 4](../../assets/images/cyberduck_4.png)

- Your CERNbox will then be mounted via Cyberduck

![CERNBox mounted via Cyberduck](../../assets/images/cyberduck_5.png)
