# Recommended access methods

The **easiest way** to access your CERNBox is to use a Web browser: simply login to your account at [https://cernbox.cern.ch](https://cernbox.cern.ch).

However, if you want to be able to continue to work with your files locally when you are offline, or to use your own applications to work on your files, use the Sync Client or Mobile App for your device.
Click [here](https://cernbox.web.cern.ch/cernbox/downloads/) for available Sync Clients and Apps. 

The following is recommended for your:

* [Windows device](#windows-device)
* [Linux device](#linux-device)
* [Mac OS device](#mac-os-device)
* [Android mobile](#android-mobile)
* [iOS mobile](#ios-mobile)


## Windows device

![windows workflow](../../assets/images/windows-workflow.svg)

!!! info Information
    If your NICE account has been migrated from DFS to CERNBox, the default folders (Desktop, Documents, Pictures, etc) will already be available on your PC. You only have to sync the other folders that you will use.

* [Sync Clients](https://cernbox.web.cern.ch/cernbox/downloads/)
* [Windows Network Share or Mapped Drive](samba.md)
* [WebDav](webdav.md)


## Linux device

![linux workflow](../../assets/images/linux-workflow.svg)

* [Sync Clients](https://cernbox.web.cern.ch/cernbox/downloads/)
* [EOS FUSE for Linux Documentation](https://eos-docs.web.cern.ch/diopside/manual/using.html#fuse-mounting-eos-with-eosxd)

Please note that in order to run the EOS FUSE mount on ALMA8/9 (containerized or not), you need to update the `default_tkt_enctypes` value in your `/etc/krb5.conf` as follows:

```
[libdefaults]
default_realm = CERN.CH
ticket_lifetime = 25h
renew_lifetime = 120h
forwardable = true
proxiable = true
default_tkt_enctypes = aes256-cts aes128-cts des3-cbc-sha1 des-cbc-md5 des-cbc-crc arcfour-hmac-md5
chpw_prompt = true
allow_weak_crypto = true
rdns = true
```

## Mac OS device

![mac os workflow](../../assets/images/mac-os-workflow.svg)

* [Sync Clients](https://cernbox.web.cern.ch/cernbox/downloads/)
* [SMB Mapped Drive](samba.md) (using `smb://` as address prefix)
* [WebDav](webdav.md)


## Android and iOS devices

Please check the [Mobile](../../mobile/index.md) section.

