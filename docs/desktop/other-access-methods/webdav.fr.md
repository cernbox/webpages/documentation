# WebDav

Une alternative est de mapper un lecteur réseau à WebDav, avec l'adresse suivante :

    `https://cernbox.cern.ch/cernbox/webdav/home`  (URL raccourcie, ne s'applique que pour l'utilisateur actuel)

Le chemin d'accès via WebDav à n'importe quel espace CERNBox est le suivant :

    Pour acceder au espace utilisateur: `https://cernbox.cern.ch/cernbox/webdav/eos/user/d/doe`  
    Pour acceder au espace project: `https://cernbox.cern.ch/cernbox/webdav/eos/project/c/cernbox`

Attention: ces liens ne sont pas à utiliser dans un browser web.

!!! warning "Suggestions/Limitations"

    - Le Finder sur MacOSX n'est pas très efficace, vous pouvez rencontrer des vitesses de téléchargement faibles suivant votre connexion Internet
    - Le File Explorer (ainsi que le Network Drive) sur Windows ne suit pas entièrement le standard WebDAV et il ne marche pas avec CERNBox
    - Dans ces cas, on vous recommende d'utiliser Cyberduck ou d'autres applications client
