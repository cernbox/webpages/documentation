# EOS and XRootD client access 

The XRootD/EOS entry point (instance server) for your files in CERNbox is `root://eosuser.cern.ch`
and your user directories are under `/eos/user/<initial>/<userid>`,  (e.g. `/eos/user/l/laman`)

- To create a directory:

`% eos mkdir /eos/user/<initial>/<userid>/<my_directory>`, (e.g. `% eos mkdir /eos/user/l/laman/office_plans`)

!!! warning "Note"

    If you are using EOS for both CERNBox and your experiment be sure to verify which instance of EOS you are querying when running EOS commands. 

    One way to force an EOS command to query a specific instance is to specify the server for that instance in the command line: `% eos <instance server> <command>`
    Exemples
      - to list laman's files in CERNBox, `% eos root://eosuser.cern.ch ls /eos/user/l/laman`
      - to query EOSCMS, `% eos root://eoscms.cern.ch <command>`
