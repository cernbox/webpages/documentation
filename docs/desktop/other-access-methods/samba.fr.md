# Windows: accès par lecteur réseau (CIFS/SMB)

Depuis une machine sous Windows ou MacOS, vous pouvez accèder directement à votre espace CERNBox à travers votre gestionnaire de fichiers, avec les lecteurs réseau standard. Un cluster de gateways est disponible pour cela.

A noter que cette fonctionalité n'est disponible que depuis le réseau local du CERN (LAN). Si vous avez besoin d'accèder a ce service depuis un réseau étendu (WAN), veuillez vous servir d'outils tel que le Sync Client ou WebDav.

!!! warning "Important"

    Quand vous naviguez les resources CERNBox avec ce methode, veuillez bien noter que dans les paramètres de sécurité, Windows montre toujours que tout le monde a tous les droits d'accès. Cela **N'EST PAS VALIDE**: les accès sont bien reglés selon les e-groups des projets (pour les projets) ainsi que ce qui est visible dans l'interface web de CERNBox!

- Se connecter via `Start` --> `Run` --> `\\cernbox-smb.cern.ch\eos\user` ou ouvrir le lecteur `H:` s'il est configuré sur votre système

![Connexion SAMBA ](../../assets/images/samba_1.png)

Vous pouvez également utiliser directement le chemin d'accès à votre CERNBox, par exemple `\\cernbox-smb.cern.ch\eos\user\d\duret`

- ou, vous pouvez mapper un lecteur réseau sur votre CERNBox, par exemple  W:

![SAMBA mappé à un lecteur réseau](../../assets/images/samba-1.png)

Vos fichiers/répertoires sont visibles dans le disque sélectionné précedemment

![SAMBA mappé à un lecteur réseau](../../assets/images/samba-2.png)
 
Déconnectez vous qund vous avez fini:

![SAMBA mappé à un lecteur réseau](../../assets/images/samba-3.png)


# Acceder à un dossier partagé avec vous

Il est possible de mapper un dossier qui a été partagé avec vous via le méthode de partage authentifié.

 - Récupérer le chemin EOS du dossier spécifique dans la section 'Partagé avec vous'. Allez au fichier qui a été partagé avec vous.
 
 - En vous servant du chemin EOS, faire un mapping sur un autre disque réseau (ex : Disque T, comme décrit ci-dessus). PS : n'oubliez pas d'utiliser `\` à la place de `/` (ex : `\\cernbox-smb\eos\user\g\gonzalhu\a_shared_folder`)

Le meme est possible pour un dossier projet, en partant d'un parcour comme `\\cernbox-drive\project\p\project_name`.

Par contre, il n'est pas possible d'acceder à un dossier partagé en lien anonyme: pour cela vous ne pouvez utiliser que l'interface web.
