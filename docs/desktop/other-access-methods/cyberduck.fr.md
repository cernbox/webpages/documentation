# Cyberduck
Vous pouvez accèder à CERNBox via Cyberduck, logiciel libre client pour for FTP et SFTP, WebDAV.  Il est disponible pour Mac OS X et Windows, sous license publique générale GPL.

- Télécharger et installer Cyberduck depuis l'adresse : [https://cyberduck.io/?l=en](https://cyberduck.io/?l=en)
- Démarrer Cyberduck et taper votre nom utilisateur et mot de passe :

![Cyberduck step 1](../../assets/images/cyberduck_1.png)

- Cliquer sur le bouton **Open Connection** :

![Cyberduck Step 2](../../assets/images/cyberduck_2.png)

- La fenêtre  **Open Connection** s'affiche, sélectionner **WebDav (Https/SSL)** dans le menu déroulant :

![Cyberduck Step 3](../../assets/images/cyberduck_3.png)

- Puis taper *cernbox.cern.ch* pour le nom du serveur
- Taper alors voter nom d'utilisateur et mot de passe
- Cliquer sur **More Options** et 
- Taper le chemin d'accès */cernbox/webdav/home*
- Puis cliquer sur le bouton **Connect**

![Cyberduck Step 4](../../assets/images/cyberduck_4.png)

-  Votre CernBox sera donc monté via Cyberduck

![CERNBox mounted via Cyberduck](../../assets/images/cyberduck_5.png)
