# Lxplus

CERNBox files are stored in EOS (the disk-based storage service), in the instance EOSUSER, to be precise . 

If you want to access your CERNBox files in EOS from **lxplus**, do the following: 

   `% cd /eos/user/<initial>/<account>`

to access your eosuser directory

