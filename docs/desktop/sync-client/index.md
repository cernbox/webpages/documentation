# Synchronisation with CERNBox

The recommended method for keeping your desktop PC synchronised with your CERNBox is by using the [CERNBox Desktop Client](https://cernbox.web.cern.ch/cernbox/downloads/). 

You can configure the CERNBox client to save files in any local directory you want, but we recommend that you specify ~/cernbox as the name of your local sync folder (the character ~ specifies the home directory on your laptop, for example). 

This process will keep your specified local desktop folder synchronised with your CERNBox, and **VICE-VERSA**, e.g. if you remove a file in one of your synced folders, it will be removed from the server as well!

The client displays the current connection status (the icon ![](../../assets/images/cloud_green.png) means that your local folder is synchronised with the server) and logs all activity, so you always know which remote files have been downloaded to your PC, and you can verify that files created and updated on your local PC are properly synchronised with the server.

The recommended method for syncing your CERNBox with Android and/or Apple iOS devices is by using the CERNBox mobile apps that you can download via [Google Play](https://play.google.com/store/apps/details?id=ch.cern.cernbox) and/or [AppStore](https://itunes.apple.com/en/app/cernbox/id1082672644).
