# Virtual File System (VFS) for Windows 

Versions after 2.9.2 include a new synchronization mode: **the Virtual File System (VFS)**. This new mode enables users to access entire file trees without the need to download them completely or manually select the subfolders of interest from the client's interface. This becomes specially useful when trying to sync project spaces and large folders, and reduces considerably the time it takes for the client to be set up.

This mode gives the user an "online file system" feeling with a native look and feel, that allows to **access its files on demand while maintaining the option to keep a subset of the files synchronized locally** as it happens with the already existing clients.

Also notice that, currently is not possible to combine together this new sync mode with the old, selective sync list for the same folder. There is more information about this limitation in [the following link](https://github.com/owncloud/client/issues/7320). The two modes can coexist for different folders under the same account, though.

## Enabling VFS for Sync Folder pairs:

There are three options to enable this new mode of synchronization on the client:

1. When **setting up your account** on the client for the first time, on the "Connection Wizard".
2. While **creating a new "Folder Sync Connection" pair** from the interface, by selecting "Use virtual files instead of downloading content immediately".
3. Or by **migrating traditional (selective) sync connection pairs** by right-clicking them and selecting the "Enable virtual file support" option. Notice that this option will recreate the entire folder structure, including the unselected folders due to the already mentioned limitation for mixing these two modes.

![Different places on which to enable VFS mode on the client](../../assets/images/desktop_vfs_enabling.png)

## Main changes on the User Interface:

All those sync folders with the VFS mode enabled are displayed with a purple ribbon on the client's main screen, clearly stating that this mode has been enabled for that folder:

![Differences in the interface while using VFS vs. Selective Sync](../../assets/images/desktop_vfs_ui.png)

The traditional "overlay icons" have been replaced by system-wide icons displayed on a new column of the Windows Explorer list view:

![Screenshot highlighting the new column on the Windows Explorer to display the status of virtual files and a dialogue with the progress of an instant download](../../assets/images/desktop_vfs_explorer.png)

A Windows native data-transfer dialogue is also displayed when opening an online file to provide the download progress as well as a time estimate for the file to open. This also applies to batch operations as well as files opened through third-party software (i.e. not from the Windows Explorer, even from the CLI/PowerShell).
New VFS file/folder status and transitions

This new mode is based on three potential file states and the transitions between these:

1. Placeholder/dehydrated files: not available offline, represent remote files that haven't been downloaded nor synchronized yet.
2. Full file: representing a file downloaded "on demand" either from the Windows Explorer or a third party software. The client keeps track of these files so if they're not accessed frequently the space can be reclaimed manually.
3. Pinned full file: this state is accessed through the right-click, contextual menu option 'Always keep on this device'. It's meant for important files that should always be kept on sync with the server.

The following graph illustrates these states, their icons and transitions:

![Highlighting new VFS entries on the context menu. States and transitions on VFS](../../assets/images/desktop_vfs_transitions_context.png)
