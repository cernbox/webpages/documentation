# CERNBox et la synchronisation

Quelque soit votre plateforme de travail (ordinateur de bureau ou ordinateur portable - Linux, Mac ou Windows - ou même téléphone portable ou tablette - Android, iOS ou Windows),  vous avez  toujours  accès à la dernière version de vos fichiers de travail en synchronisant cette plateforme avec votre CERNBox. 

Maintenez votre ordinateur de bureau synchronisé avec votre CERNBox en installant [l'application de bureau CERNBox](https://cernbox.web.cern.ch/cernbox/downloads/).

Vous pouvez configurer l'application pour sauvegarder vos fichiers en local dans n'importe quel dossier, mais pour plus de facilité nous recommandons de laisser le choix par défaut ~/cernbox (le ~ représente le répertoire Home de votre ordinateur). Ce processus  permet de rendre tous les fichiers contenus dans votre dossier CERNBox accessibles sur tous les appareils sur lesquels vous êtes connecté à votre compte CERNBox. Toutes les modifications (révisions, ajouts ou suppressions) que vous apportez aux fichiers de votre dossier CERNBox sont synchronisées, etd **VICE-VERSA**, c.a.d. si vous supprimez un fichier de votre dossier synchronisé, il sera également supprimé sur le serveur qui héberge votre CERNBox.

L'application de bureau CERNBox  vous informe de l'état de votre connexion (le pictogramme ![](../../assets/images/cloud_green.png) signifie que votre dossier local est correctement synchronisé avec le serveur) and enregistre toute l'activité sur le dossier synchronisé : vous pouvez ainsi vérifier que  les fichiers sur le serveur ont été téléchargés sur votre PC, et inversement que les fichiers que vous avez créés et modifiés en local ont été correctement synchronisés sur le serveur.

Pour synchroniser vos smartphones ou tablettes Android et ou Apple iOS, utilisez les applications clients que vous pouvez télécharger via [Google Play](https://play.google.com/store/apps/details?id=ch.cern.cernbox) et/ou [AppStore](https://itunes.apple.com/en/app/cernbox/id1082672644).
