# Sync a shared folder
In the previous sections you have learned how to synchronise your own CERNBox folders to your local device(s). 

When it comes to accessing folders that have been shared with you by colleagues, you might look and download them just that one time, but if the files and folders are **constantly being modified** you can decide to set up a synchronisation (sync) to your desktop, say, so that you will have the latest modifications. 


<iframe width="576" height="360" frameborder="0" src="https://weblecture-player.web.cern.ch/?year=2023&id=1275637" ?showtitle="true" allowfullscreen></iframe>
[Tutorial about Syncing a Share on twiki.cern.ch](https://twiki.cern.ch/twiki/bin/view/Edutech/CERNBoxSyncAShare){:target="_blank"}



Check that you know exactly what will be synchronised with you. In particular, make sure that you have enough space on your local device and that the connectivity will handle the download.

* Go to the files that have been **_Shared with you_**
* For the folder that you want synchronised, retrieve the _**Fuse Path**_ to it: click on :material-dots-vertical: (the 3 dots), select ***Details*** and copy the path.

![Retrieve EOS path](../../assets/images/fuse-path.png)

* Make sure that you have a local folder that will be used for the synchronisation; if you do not have one create one on your local host.
* Click on the CERNBox Sync Client icon to open the Client window. You will see the synchronisation of your CERNBox home that you had set up initially.
* Now click on the ***Add Folder Sync Connection***  button.
 
![click Add Folder Sync Connection](../../assets/images/how-to-sync-shared-folder-7.png) 

![Choose your local folder for sync](../../assets/images/how-to-sync-shared-folder-10.png) 

* PASTE the path of the shared folder that you want to have synced

![paste the shared folder path](../../assets/images/how-to-sync-shared-folder-11.png) 

> **Important**
>
> Do not navigate to the path via the folders tree. Do not select "*Home*" at this stage as you will synchronise your CERNBox home into another folder!

![choose sub-folder, then sync](../../assets/images/how-to-sync-shared-folder-12.png) 

You can see what you are synching:

![what you are synching](../../assets/images/how-to-sync-shared-folder-13.png) 

![can see more info for syncs](../../assets/images/how-to-sync-shared-folder-15.png) 

