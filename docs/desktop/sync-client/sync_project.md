# Sync a project

Follow steps 2->10 to synchronise a remote project to one of your local folders. You might already have been given the **path** of the project's location, but otherwise you can **copy** it from the Web browser view to your CERNBox:

![](../../assets/images/proj-space-sync-6.png)

Open your CERNBox Sync Client to set up the project->local folder synchronisation. Click on the CERNBox Sync Client icon ![](../../assets/images/cloud_green.png) to open it - if this was not running previously, you will have to start it.

![](../../assets/images/proj-space-sync-8.png)

![](../../assets/images/proj-space-sync-11.png)

**Paste** the path that you copied:

![](../../assets/images/proj-space-sync-12.png)

![](../../assets/images/proj-space-sync-16.png)

![](../../assets/images/proj-space-sync-18.png)

![](../../assets/images/proj-space-sync-19.png)

![](../../assets/images/proj-space-sync-24.png)

## Sync one specific project folder locally

Instead of synchronising the complete folder to your local folder, you may decide to synchronise a **SPECIFIC** folder only. 

Open your CERNBox Sync Client, click on 'Add Folder Sync Connection', and follow steps 1->7. In our example, the folder 'microtests' in the remote project 'noafs' is being synced to the local folder 'no-afs-microtests'

![](../../assets/images/proj-space-sync-41.png)

![](../../assets/images/proj-space-sync-42.png)

![](../../assets/images/proj-space-sync-43.png)

![](../../assets/images/proj-space-sync-44.png)

