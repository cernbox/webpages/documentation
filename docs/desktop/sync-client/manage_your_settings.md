# Manage your settings
CERNBox provides a few parameters that you can adjust for fine-tuning of the synchronisation between the server and your local device.
You have access to your current synchronisation settings by:

- clicking on the icon ![](../../assets/images/cloud_green.png) in your taskbar<br>
![CERNBox sync client in Windows taskbar](../../assets/images/sync_settings_1.png)
- and choosing the **General** tab<br>
![Sync client Settings screen](../../assets/images/sync_settings_2.png)

### General Settings

- It is recommended to keep the tick for the sync client to start when you start your local device.

- When the **Show Desktop Notifications** box is ticked, all activity between the server and your local device is shown (for Windows, on the lower right corner of the screen) when happening.

### Advanced settings

- Click on the **Edit Ignored files** button to select which type of files you don't want to sync.<br>
   - By default, temporary files are not synced. Scroll down the list to see which type of files are currently excluded from sync<br>
![Sync ignored files screen](../../assets/images/sync_settings_3.png)<br>
   - Click on the **Add** button and type any pattern you want to ignore during the sync. This could be for files or for folders or sub-folders. <br>![Add new pattern to ignore file]
(../../assets/images/sync_settings_4.png)<br>Click on **OK**. You can repeat the procedure to add more patterns to be ignored.
- In the following example, files with the .git extension will be ignored. This functionality is quite useful when some application is producing a lot of temporary files that you never need to keep.<br>![Example: New file pattern ignored](../../assets/images/sync_settings_5.png)<br>
- You might decide to sync ALL the hidden files (not recommended). Then tick the **Sync hidden files** box on the top of the window.
<br><br>

- To prevent any uncontrolled activity between the server and your local equipement , you might want to be informed before any big amount of data is downloaded on your local equipement. By default, the warning and request for confirmation limit is set to 500 MB. Change it if you wish so.

<a id="KB0002983"></a>
### Version
Open the CERNBox desktop synchronisation client via the icon ![](../../assets/images/cloud_green.png) in your taskbar.

 Under 'About' in the **General** tab you should see version 2.1.1 or greater.<br> 
![Sync client version](../../assets/images/desktop_sync_version.png)

If it is not the case, please install the latest version that you can find on the page [CERNBox Desktop Client](https://cernbox.cern.ch/cernbox/doc/clients.html).