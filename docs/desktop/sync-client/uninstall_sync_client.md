# Uninstaller sync client

## on MacOSX

There is no standard uninstaller for Mac applications (unlike Windows). For manual application cleanup please:

1. remove CERNBox from Applications folder (`/Applications/cernbox.app`)
2. remove CERNBox from Mac's system preferences >> `Users&Groups` >> `Login Items` Tab
3. remove auxiliary directories:
  - `~/Library/Application Support/CERNBox`
  - `~/Library/Preferences/ch.cernbox.client.plist`
  - `~/Library/Caches/ch.cernbox.client`
