# Utilisation de l'application de synchronisation

L'application de bureau CERNBox Bureau reste en arrière-plan et est visible sous forme d'icône dans la barre d'état système (Windows, KDE), barre d'état (Mac OS X), ou dans la zone de notification (Linux).
L'indicateur d'état utilise des icônes de recouvrement pour indiquer l'état actuel de votre synchronisation.

| Icône     | Information  |
|-----------|--------------|
| ![](../../assets/images/state-ok-32.png) | Le cercle vert avec la coche blanche vous indique que votre synchronisation est active et que vous êtes connecté à votre serveur CERNBox |
| ![](../../assets/images/state-sync-32.png) | L'icône de recouvrement bleue avec les demi-cercles blancs signifie que la synchronisation est en cours |
| ![](../../assets/images/state-pause-32.png) | L'icône de recouvrement jaune avec les lignes parallèles vous indique que la synchronisation a été interrompue, très probablement par vous-même ! |
| ![](../../assets/images/state-offline-32.png) | L'icône de recouvrement grise avec trois points blancs signifie que votre client de synchronisation a perdu sa connexion avec le serveur CERNBox |
| ![](../../assets/images/state-information-32.png) | Quand vous voyez un cercle blanc avec la lettre "i" qui est l'icône d'information, c'est que CERNBox a quelque chose à vous dire. Cliquer sur l'îcone pour voir ce qu'il a à vous dire |
| ![](../../assets/images/state-error-32.png) | Le cercle rouge avec le "x" blanc indique une erreur de configuration, par exemple une erreur de connexion ou une URL incorrecte pour le serveur |


Les changements effectués dans votre dossier local sont immédiatement répercutés sur le serveur. Par contre, le téléchargement en local des changements directement effectués sur le serveur  s'effectue toutes les 30 secondes. 
    

Faites un clic droit sur l'icône du client de synchronisation (dans la barre d'état) pour ouvrir le menu suivant :

![sync_right_click](../../assets/images/sync_right_click.png)
* Accèder **EN LIGNE** à votre CERNBox via un navigateur Web
* Accèder à votre dossier **LOCAL** CERNBox à travers un gestionnaire de fichiers
* Etat de la synchronisation
* Les modifications récentes, en montrant les dernières activités du serveur
* Paramètres de la synchronisation
* Se déconnecter, arrêter toutes les opérations de synchronisation
* Quitter CERNbox, en déconnectant et fermant le client de bureau.


