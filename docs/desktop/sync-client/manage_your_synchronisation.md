# Manage your synchronisation

- A left-click on the Desktop Sync Client icon (in the taskbar) opens the CERNBox settings window, which, by default, gives the list of folders to synchronise on your account.

![Manage your sync](../../assets/images/sync_left_click.png)

By default the folder sync tree is expanded and sync folders are marked with a ticked box &#x2611;. 
In the example above, the folder `dorothee_test` is kept on CERNBox (as an archive) but is not available on your local desktop for offline use.

- Tick or untick the box beside the folders you want to sync or unsync and then click on the ***Apply*** button. Unchecked folders will be removed from your local file system and will not be synchronised to this computer anymore.

![Choose what to sync](../../assets/images/choose_sync.png)

This functionality is quite useful when you want to archive some data on the server (no need to keep all your stuff on your local disk) or when you do not have so much space on your local disk.

- When you click on the ![](../../assets/images/3_dot.png) button on the right beside a CERNBox folder (in the above example, the 'home' folder), it opens a dropdown menu to:

![](../../assets/images/sync_menu.png)

| Choice | Fonction |
| ------ | -------- |
| **Open folder** | open the chosen folder with the file system of your device.
| **Choose what to sync** | will expand your folder tree (if not already done). Then refer to the procedure as described above to choose which folder you want to synchronise or which folder you want to remove from the synchronisation.
| **Pause/Resume sync** | **Pause sync** pauses synchronisation operations without making any changes to your account. It will continue to update file and folder lists, without downloading or updating files. Click on **Resume sync** to resume sync operations. To stop all sync activitiy use **Remove folder sync connection**. 
| **Remove folder sync connection** | remove the sync connection without removing the account. This stops all sync activity, including file and folder list updates. If you want to synchronize the folder tree again then click the **Add folder sync connection** button, and re-select the folder tree that you want to sync.
 
Files and folder icons on your  **LOCAL** disk have changed: overlay icons, as defined above, apply on the files and folders icons.
For example:

![Example: LOCAL folder with overlayed icons](../../assets/images/cernbox_local_icon.png)
