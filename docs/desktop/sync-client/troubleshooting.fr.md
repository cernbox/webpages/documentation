# Résolution de problème

CERNBox fournit certains outils pour suivre l'activité de synchronisation entre votre équipement local et le server.

## Quand tout va bien
* Cliquez sur l'icône du client CERNBox  (en espérant que ce soit celle-ci ![](../../assets/images/cloud_green.png)), et
* Cliquez sur l'option **Activity**, puis
* Choisissez l'onglet **Sync Protocol**.

![Activités de synchronisation](../../assets/images/sync_activity_f.png)

Vous obtenez ainsi la liste de toutes les activités entre votre équipement local et le serveur, depuis votre dernière connexion. Voici quelques explications des termes employés :

- **Upload** (a1) : Le fichier local a été modifié depuis la dernière connexion. La nouvelle version est copiée sur le système de stockage de données EOS
- **Download** (a2) : La dernière version valable est celle sur le système de stockage de données EOS. Elle est téléchargée sur votre équipement local.
- **Delete** (a3) : Le fichier dans votre répertoire local a été renommé ou supprimé. La copie sur le système de stockage de données EOS est supprimée .
- **Moved to. . .** (a4) : Un fichier ayant un nom qui existe déjà  sur le système de stockage de données EOS a été créé dans votre répertoire local. Il est copié et remplace la version sur le système de stockage de données EOS
- **Download** (a5) : Quand un fichier est modifié directement sur le système de stockage des données EOS (voir a4), la nouvelle version est téléchargée sur votre équipement local.

## Avez-vous un problème ?
* Cliquez sur l'icône du client CERNBox , et
* Cliquez sur l'option **Activity**, puis
* Choisissez l'onglet  **Not Synced** 

![Diagnostic de non synchronisation](../../assets/images/sync_ignored_f.png)

Cela donne la liste des fichiers ignorés ou erronés

Vous obtenez alors la liste des fichiers qui ont été ignorés pendant la synchronisation et ceux pour lesquels une erreur s'est produite.

## Raccourci Clavier
- F6 force la synchonisation
- F12 ouvre la fenêtre de log.


## Fichiers de conflit

Il est aussi possible que des fichiers au nom étrange apparaissent dans vos dossiers synchronisés. C'est signe que quelque chose s'est mal passé !

![Problème de synchronisation](../../assets/images/sync_pb.png)

Un fichier marqué comme modifié sur le serveur et le client qu moment de la synchronisation resultera en un fichier de conflit. Le fichier modifié localement est renomé en fichier de conflit et la version du serveur est téléchargée avec le nom original

Le fichier avec le conflit est pas mergé automatiquement. C'est à l'utilisateur de faire la comparaison entre les deux versions et de résoudre les différences. Une fois que le contenu est mergé (si necessaire), l'utilisateur peut supprimer le fichier de conflit

Les fichier de conflits ont le format suivant : `[nom_de_base]_conflict-[date]-[heure].[ext]`

Par exemple : `nbsignatures_conflict-20161208-112100.db`

Pour obtenir le liste complète des fichiers de conflit : 

`% find ~/cernbox -name '\*_conflict-\*-\*'`


## Renomage de fichier partagé

Si vous synchronisez un dossier qui est 'partagé avec vous' en local, et si la personne qui l'a partagé avec vous le renomme, vous verrez une erreur ('Csync failed due to unhandled permission..').

![](../../assets/images/sync-error-following-name-change-1.png)

Pour voir le contenu du dossier renommé, vous devez faire une nouvelle syncronisation avec le nouveau nom.

Si vous acceder le dossier par le Web, le nom du dossier garde son nom original mais le chemin dans 'details' change pour montrer le nouvel emplacement. Rafraîchissez le cache du navigateur pour voir les changements (F5).


## Renaming/removing the local folder of a sync pair

If you have moved away the top folder where a synchronization was established, it could happen that the sync client still shows a sync pair in bad state and unusable. In this case, if you just want to clean up your configuration you have to edit the configuration file: for that, quit your sync client, edit your file, and restart the sync client afterwards.

The configuration file of the sync client is located at:

- `%APPDATA%\CERNBox\cernbox.cfg` on a Windows system
- `/Users/your_user_name/Library/Preferences/cernbox/cernbox.cfg` on a Mac OS system
- `/home/your_user_name/.config/cernbox/cernbox.cfg` on a Linux system

> Warning: you are adviced to keep a copy of the configuration file prior to editing it. Modifying by hand the configuration file can lead to unexpected results. Please refrain from doing so if you are not sure about it.


## Broken Symlinks

If you have broken symlinks in /eos/user/y/yourhome then you might see an error like this:

![](../../assets/images/symlink-error.png)


A broken symlink will affect the synchronisation of the directory which contains it. Not only will the symlink not be synchronised, but all the other files/folders in that directory will NOT be synchronised.

To get rid of the synchronisation error you should remove broken links.

For some editors, such as emacs, broken symlinks are created as lock files. Hence, if you run emacs on lxplus, then in /eos/user/<path to your emacs file> the broken symlink will affect the file synchronisation of the Sync Client as long as the editor is open and contains unsaved modifications in the file that is being edited.


#### Valid symlinks

Please note that not all valid symlinks are followed transparently. Make sure to NOT point symlinks back to the top of the tree as this can create an infinite recursion.

If you do not want the symlinks to be synchronised at all, you should add these symlinks to the excluded file list pattern in the Sync Client. 

It would be best to have one exclusion rule for all symlinks that you do not want to synchronise. For example, you might use a naming convention which is sufficiently unique to identify links that you do not want to synchronise, e.g. "*.link", or similar.

Go to the Sync Client ,  and then go to General>Edit Ignored Files>Add

 ![](../../assets/images/symlink.png)



##### Recommended workflow to do with symlinks:

Symlinks (maybe they were created in lxplus) are not followed through in CERNBox (via the Sync Client/Web Interface), in-fact, they are not visible in CERNBox. For an equivalent workflow, you are recommended to have the folder shared to you, and to then sync that folder to your CERNBox space.

The folder has to be shared with you:
[](../../web/sharing/index.md)

You then have to sync the other person's folder to your CERNBox space:
[](../sync-client/add_a_folder_sync_connection.md)