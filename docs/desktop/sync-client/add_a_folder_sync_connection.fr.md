## Synchroniser un répertoire

En ce qui concerne l'accès aux dossiers et fichiers qui ont été partagés avec vous, vous pouvez choisir  de les télécharger qu'une fois. Mais si les fichiers et les dossiers sont **constamment modifiés**, vous pourriez décider de mettre en place une synchronisation (sync) sur votre ordinateur de bureau afin d'avoir toujours accès aux dernières versions.

Vous pouvez regarder la [video](https://cds.cern.ch/video/2288369) ci-dessous ou lire le texte qui suit.

<iframe width="576" height="360" frameborder="0" src="https://weblecture-player.web.cern.ch/?year=2023&id=1275637" ?showtitle="true" allowfullscreen></iframe>
[Tutoriel sur la synchronisation d'un partage sur twiki.cern.ch](https://twiki.cern.ch/twiki/bin/view/Edutech/CERNBoxSyncAShare){:target="_blank"}


* Tout d'abord, que savez-vous exactement sur ce que vous voulez synchroniser ? En particulier , assurez-vous que vous avez suffisamment d'espace sur votre ordinateur de bureau et que la connectivité peut gérer le téléchargement.

![Synchroniser un dossier - 1](../../assets/images/fuse-path.png)

* Récupérez le chemin d'accès Fuse à ce dossier en cliquant sur ***Détails*** sur :material-dots-vertical:
* Créez un dossier local sur votre ordinateur de bureau, dans lequel vous allez synchroniser ce nouveau répertoire.
* Cliquez sur l'icône du client CERNBox ![](../../assets/images/cloud_green.png)
* Et cliquez sur le bouton ***Add Folder Sync Connection***, en bas à gauche de cette fenêtre. 

![Synchronisaer un dossier -2 ](../../assets/images/sync_add_folder_1_f.png)

* Retrouvez le dossier que vous venez de créer dans votre arborescence et cliquez sur le bouton ![](../../assets/images/next_button.png).
* Tapez directement le chemin d'accès EOS que vous avez récupéré plus tôt.

![Identifier le dossier à synchroniser](../../assets/images/sync_add_folder_2_f.png)

> **[info] Note importante**
>
> Vous synchronisez à nouveau dans un autre dossier votre propre CERNBox si vous choisissez "***Home***" à l'étape précédente

* Cliquez sur le bouton ![](../../assets/images/next_button.png) pour démarrer la synchronisation.<br>Lorsqu'elle sera terminée, vous aurez alors l'écran suivant :

![Synchroniser un dossier - 3](../../assets/images/sync_add_folder_3_f.png)

* Si vous le désirez, vous pouvez ne synchroniser que quelques dossiers de ce répertoire comme expliqué précédemment pour votre répertoire CERNBox. 
