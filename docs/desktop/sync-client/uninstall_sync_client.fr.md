# Uninstall sync client

# on MacOSX

Au contraire de Windows, les applications Mac ne peuvent pas être desinstallées automatiquement. Pour supprimer l'application client de synchronisation manuellement, vous devez :

1. enlever CERNBox du dossier Applications  (`/Applications/cernbox.app`)
2. enlever CERNBox des préférence système >> `Users&Groups` >> `Login Items` Tab
3. supprimer les répertoires auxillaires :
  - `~/Library/Application Support/CERNBox`
  - `~/Library/Preferences/ch.cernbox.client.plist`
  - `~/Library/Caches/ch.cernbox.client`
