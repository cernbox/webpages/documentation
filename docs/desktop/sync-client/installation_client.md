# Install Desktop Sync Client

<iframe width="576" height="360" frameborder="0" src="https://weblecture-player.web.cern.ch/?year=2023&amp;id=1275596" ?showtitle="true" allowfullscreen></iframe>
[Tutorial about CERNBox Client Install on twiki.cern.ch](https://twiki.cern.ch/twiki/bin/view/Edutech/CERNBoxClientInstall){:target="_blank"}  

<br>

Download the latest version of the CERNBox Desktop Synchronisation Client from the  [CERNBox Desktop Client](https://cernbox.web.cern.ch/cernbox/downloads/) page.

Installation on Mac OS X and Windows is the same as for any software application: download the program, double-click it to launch the installation, and then follow the installation wizard.

Specific package management systems are used to install the CERNBox client sofware on Linux machines.


**NOTE** After you have installed the Desktop Sync Client, the window to configure your Client will look like the one below. Do NOT 'Click here' as this will go to an error page. This link is from the original software, and is not part of the CERN functionality.

![app-password link](../../assets/images/app-passwd-login-new.png)
