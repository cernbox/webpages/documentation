# Installer l'application de bureau CERNBox

<iframe width="576" height="360" frameborder="0" src="https://weblecture-player.web.cern.ch/?year=2023&amp;id=1275596" ?showtitle="true" allowfullscreen></iframe>
[Tutoriel sur l'installation du client CERNBox sur twiki.cern.ch](https://twiki.cern.ch/twiki/bin/view/Edutech/CERNBoxClientInstall){:target="_blank"}  

Téléchargez la dernière version de l'application de bureau pour la synchronisation CERNBox  depuis la page [CERNBox Desktop Client](https://cernbox.cern.ch/cernbox/doc/clients.html).

L'installation de ce logiciel pour Mac OS X et Windows se fait comme pour n'importe quel autre logiciel : téléchargez le programme, cliquez deux fois sur l'icône pour démarrer l'installation et suivez les instructions.

Des procédures spécifiques pour les machines Linux sont disponibles.

Les instructions pour les différentes configurations de votre ordinateur de bureau se trouvent :

* pour Mac OS X : [https://cernbox.cern.ch/cernbox/doc/postinstall/osx.html](https://cernbox.cern.ch/cernbox/doc/postinstall/osx.html)
* pour Windows : [https://cernbox.cern.ch/cernbox/doc/postinstall/windows8.html](https://cernbox.cern.ch/cernbox/doc/postinstall/windows8.html)
* pour Linux : [https://cernbox.cern.ch/cernbox/doc/linux.html](https://cernbox.cern.ch/cernbox/doc/linux.html)

Vous pouvez regarde le [video](https://cds.cern.ch/video/2288368) d'e-learning sur comment installer le client pour ordinateur présentée ci dessous. 

<video type="video/mp4" width="640" height="360" poster="/assets/images/e-learning-poster-2.png" controls="controls" <source src="https://mediastream.cern.ch/MediaArchive/Video/Public/Conferences/2017/669067/669067-2000-kbps-1280x720-23.98-fps-audio-96-kbps-44-kHz-stereo.mp4"/></video>
