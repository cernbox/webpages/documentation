# Use the Synchronisation Client

The CERNBox Desktop Sync Client remains in the background and is visible as an icon in the system tray (Windows, KDE), status bar (Mac OS X), or notification area (Linux). The status indicator uses overlay icons to indicate the current status of your synchronization. 

| Icon     |      Meaning      |
|----------|-------------------|
| ![](../../assets/images/state-ok-32.png) | The green circle with the white checkmark tells you that your synchronization is current and you are connected to your CERNBox server. |
| ![](../../assets/images/state-sync-32.png) | The blue icon with the white semi-circles means synchronization is in progress. |
| ![](../../assets/images/state-pause-32.png) | The yellow overlay icon with the parallel lines tells you your synchronization has been paused. (Most likely by you.) |
| ![](../../assets/images/state-offline-32.png) | The gray icon with three white dots means your sync client has lost its connection with your CERNBox server. |
| ![](../../assets/images/state-information-32.png) | When you see a white circle with the letter “i” that is the informational icon, so you should click it to see what it has to tell you. |
| ![](../../assets/images/state-error-32.png) | The red circle with the white “x” indicates a configuration error, such as an incorrect login or server URL. |

Changes on your local folder are immediately uploaded onto the server. A local download of changes made on the server side is done every 30 seconds. 
    
A right-click on the Desktop Sync Client icon (in the taskbar) opens a menu with the following options: 

![](../../assets/images/sync_right_click.png)

* **Open CERNBox in browser** : access your CERNBox **ONLINE** via a Web browser
* **Open folder 'cernbox'**: access your **LOCAL** CERNBox via the file manager
* **Up to date**: status of synchronisation of your folders/files
* **Recent Changes**: latest activities
* **Settings...**: go to settings window with choice of tabs
* **Log out**: stop all synchronisation operations
* **Quit CERNbox**: log out and close the desktop client
