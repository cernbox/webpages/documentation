# Gérer vos paramètres

CERNBox permet d'ajuster certains paramètres pour optimiser votre utilisation du client de synchronisation.<br>
Vous pouvez accèder à ces paramètres en :


- cliquant sur l'icône ![](../../assets/images/cloud_green.png) dans votre barre d'état

![](../../assets/images/sync_settings_1_f.png)

- et en choississant l'onglet **General** 

 ![Paramètres de snchronisation](../../assets/images/sync_settings_2.png)

## Paramètres généraux (Global Settings)

- Il est recommandé de démarrer votre client de synchronisation en même temps que votre équipement local. C'est la valeur par défaut.

- Lorsque la case **Show Desktop Notifications** est cochée, toute activité entre votre équipement local et le serveur apparaît dans une bulle de conversation (pour Windows, dans le coin en bas à droite de votre écran).

## Paramètres avancés

- Cliquer sur le bouton **Edit Ignored files** pour définir le type des fichiers/dossiers qui ne seront pas synchronisés entre le serveur et votre équipement local.

 - Certains types, en particulier des fichiers temporaires, ne sont pas synchronisés par défaut. Vous pouvez en parcourir la liste en déroulant la fenêtre

  ![Fichiers ignorés lors de la synchronisation](../../assets/images/sync_settings_3.png)

 - Pour ajouter de nouveau type de fichier ou même un dossier complet, cliquer sur le bouton **Add**

  ![Ajouter un type de fichier à ignorer](../../assets/images/sync_settings_4_f.png)

Click on **OK**. Répèter la procédure autant de fois que nécessaire.

 - Dans l'exemple qui suit, les fichiers dont l'extension est .git seront ignorés. La possibilité d'exclure des fichiers ou des dossiers lors de la synchronisation entre votre équipement local et le serveur est particulièrement utile quand vous utilisez des applications qui génèrent beaucoup de fichiers temporaires dont il n'est pas utile de conserver la moindre version.<br>![Exemple de nouveau fichier ignoré](../../assets/images/sync_settings_5.png)<br>

 - Ce n'est pas recommandé, mais vous pouvez si besoin synchroniser TOUS les fichiers cachés. Pour cela, il faut cocher la case  **Sync hidden files** en haut de la fenêtre.<br>

 - Cliquer sur le bouton **OK** en bas de la fenêtre quand les modifications sont terminées.<br><br>


- Pour empêcher tout téléchargement incongru sur votre équipement local, vous pouvez être informé quand un téléchargement dépasse une certaine limite, par défaut 500 MB. Changez cette valeur si besoin.

## Version
Ouvrir l'application client de synchronisation CERNBox  via le pictogramme ![](../../assets/images/cloud_green.png) dans votre barre d'état.

 Sous la rubrique 'About' du menu **General**, vous trouvez le numéro de la version,  2.1.1 ou plus.

![Version du client de synchronisation](../../assets/images/desktop_sync_version.png)<br>
Si ce n'est pas le cas, vous devez installer la nouvelle version du client que vous trouvez depuis la page [CERNBox Desktop Client](https://cernbox.cern.ch/cernbox/doc/clients.html).
