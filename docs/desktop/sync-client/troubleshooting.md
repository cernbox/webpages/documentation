# Troubleshooting

CERNBox provides some tools to help you following what actions are performed for the synchronisation between your current device and the CERNBox server.

## When it goes well
* Click on the CERNBox client icon (hopefully it is the ![](../../assets/images/cloud_green.png) one), and
* Click on the **Activity** tab on the page, then
* Choose the ![](../../assets/images/cloud_green.png) **Sync Protocol** tab.

![Syn activity](../../assets/images/sync_activity.png)

This gives the list of the latest sync operations (since the last reconnection of your device to the network):

- **Upload** (a1): The file has been modified on the local desktop since the last sync. The new version is copied on the EOS Storage.
- **Download** (a2) : The latest version is the one on the EOS Storage. It is copied on your local folder
- **Delete** (a3) : The file on your local folder has been deleted or renamed. The file is deleted from the EOS storage.
- **Moved to . . .** (a4) : A file with an existing name on the EOS storage has been created on your local folder. It is copied and replaces the version in EOS storage
- **Download**  (a5) : As the file on the EOS storage has been modified (see a4), a new version is downloaded on your local folder.

## What might be a problem

* Click on the CERNBox client icon, and
* Click on the **Activity** tab on the page, then
* Choose the ![](../../assets/images/state-pause-32.png) **Not Synced**  tab.

![Sync ignored/erroneous files report](../../assets/images/sync_ignored.png)

This gives the list of ignored or erroneous files.

## Keyboard shortcuts 

F6 forces the re-sync.

F12 opens the log window (for user support data).


## Conflict files

If you see some strange file names, something went wrong!

![Example: something went wrong during sync](../../assets/images/sync_pb.png)

A file that is marked as modified on the server and at the client at the time of synchronisation will result in a conflict file. Locally modified file is renamed into a conflict file on the client and the server version of the file is downloaded with the original name. 

Conflicting content is not merged automatically because it cannot be done. It is up to the user to compare the conflict file with the original file and resolve any differences. Once the content is merged (if needed) the  user may delete the local conflict file.

Conflict files have the format: `[basename]_conflict-[date]-[time].[ext]`

For example: `nbsignatures_conflict-20161208-112100.db`

To get the full list of conflict files: 

`% find ~/cernbox -name '\*_conflict-\*-\*'`


## Renaming/moving shared folders

If you are syncing a folder (that was 'Shared with you') to your local filesystem, and then the person who shared the folder with you renames or moves it, you will get an error ('Csync failed due to unhandled permission..'). 

![](../../assets/images/sync-error-following-name-change-1.png)

To see the contents of the renamed folder or the folder that is now in its new path, you will have to set up a new synchronisation to the 'new' folder.

If you were accessing the folder by the Web, you will still see the original folder name, but the path in 'details' will change to reflect the new name or the new path. Remember to refresh the cache of the Web browser to see the changes (press F5)


## Renaming/removing the local folder of a sync pair

If you have moved away the top folder where a synchronization was established, it could happen that the sync client still shows a sync pair in bad state and unusable. In this case, if you just want to clean up your configuration you have to edit the configuration file: for that, quit your sync client, edit your file, and restart the sync client afterwards.

The configuration file of the sync client is located at:

- `%APPDATA%\CERNBox\cernbox.cfg` on a Windows system
- `/Users/your_user_name/Library/Preferences/cernbox/cernbox.cfg` on a Mac OS system
- `/home/your_user_name/.config/cernbox/cernbox.cfg` on a Linux system

> Warning: you are adviced to keep a copy of the configuration file prior to editing it. Modifying by hand the configuration file can lead to unexpected results. Please refrain from doing so if you are not sure about it.


## Broken Symlinks

If you have broken symlinks in /eos/user/y/yourhome then you might see an error like this:

 

![](../../assets/images/symlink-error.png)



A broken symlink will affect the synchronisation of the directory which contains it. Not only will the symlink not be synchronised, but all the other files/folders in that directory will NOT be synchronised.

To get rid of the synchronisation error you should remove broken links.

For some editors, such as emacs, broken symlinks are created as lock files. Hence, if you run emacs on lxplus, then in /eos/user/<path to your emacs file> the broken symlink will affect the file synchronisation of the Sync Client as long as the editor is open and contains unsaved modifications in the file that is being edited.

#### Valid symlinks

Please note that not all valid symlinks are followed transparently. Make sure to NOT point symlinks back to the top of the tree as this can create an infinite recursion.

If you do not want the symlinks to be synchronised at all, you should add these symlinks to the excluded file list pattern in the Sync Client. 

It would be best to have one exclusion rule for all symlinks that you do not want to synchronise. For example, you might use a naming convention which is sufficiently unique to identify links that you do not want to synchronise, e.g. "*.link", or similar.

Go to the Sync Client ,  and then go to General>Edit Ignored Files>Add

 ![](../../assets/images/symlink.png)


##### Recommended workflow to do with symlinks:

Symlinks (maybe they were created in lxplus) are not followed through in CERNBox (via the Sync Client/Web Interface), in-fact, they are not visible in CERNBox. For an equivalent workflow, you are recommended to have the folder shared to you, and to then sync that folder to your CERNBox space.

The folder has to be shared with you:
[](../../web/sharing/index.md)

You then have to sync the other person's folder to your CERNBox space:
[](../sync-client/add_a_folder_sync_connection.md)
