# Gérer votre synchronisation

- Faites un clic gauche sur l'icône du client de synchronisation (dans la barre d'état) pour ouvrir la fenêtre des paramètres de synchronisation. Par défaut, elle s'ouvre sur la liste des dossiers synchronisés.

![Dossiers synchronisés](../../assets/images/sync_left_click.png)

Votre répertoire par défaut (Home) est ouvert et les dossiers qui sont synchronisés sont marqués d'une coche &#x2611;. 
Dans notre exemple, le dossier `dorothee_test` est présent dans votre CERNBox (archivé, si vous préférez), mais n'est pas accessible sur votre ordinateur de bureau pour une utilisation hors ligne.

- Cochez les dossiers que vous voulez synchroniser puis appuyez sur le bouton ***Apply***. Les dossiers qui ne sont pas cochés sont supprimés sur votre disque dur local et ne sont pas synchronisés.

![Synchronisation partielle dún dossier](../../assets/images/choose_sync.png)

Cette fonctionalité est très utile pour archiver sur le serveur les fichiers/dossiers dont vous n'avez plus besoin en local, ou lorsque vous ne disposez pas de suffisamment d'espace sur votre disque dur.

Si vous cliquez sur le bouton ![](../../assets/images/3_dot.png), tout à droite du nom du dossier (ici le dossier Home), vous accèdez aux fonctions suivantes :

![sync_menu](../../assets/images/sync_menu.png)

| Choix | Fonction |
| ----- | -------- |
| **Open folder** | Ouvrir le dossier via votre gestionnaire de fichiers |
| **Choose what to sync** | Cette fonction ouvre le répertoire du dossier (si ce n'est déjà fait) et vous permet de choisir ce que vous voulez synchroniser (comme expliqué ci-dessus) |
| **Pause/Resume sync** | **Pause sync** permet de suspendre la synchronisation.La liste des fichiers et des dossiers continue à être maintenue  par CERNBox  mais plus aucun téléchargement ne s'effectue. Cliquez sur **Resume sync** pour reprendre les opérations de synchronisation |
| **Remove folder sync connection** | arrêter la synchronisation d'un répertoire. Dans les faits, toute activité de synchronisation est supprimée, y compris le maintien de la liste des  fichiers modifiés, mais le répertoire n'est pas supprimé en local. Pour synchroniser à nouveau ce répertoire, il faut reprendre la procédure d'ajout, comme expliqué ci-après |
 

Noter que les icônes des fichiers et dossiers de votre disque **LOCAL** ont changées : les icônes  de recouvrement, telles que définies ci-dessus, sont appliquées sur les îcones des fichiers et dossiers.
Par exemple, 

![Synchronisation vue dans le gestionnaire de fichiers local](../../assets/images/cernbox_local_icon.png)
