# Corner Cases

This page describes a number of known corner cases and limitations currently present in CERNBox.

## Public links and direct shares

If you're given access to a resource (file or folder) via both a public link and a direct authenticated share, and you open it via public link, the permissions associated with the authenticated share are not taken into account, despite the web frontend may show in the top-right corner your name initials. This also applies for resources in project spaces you have access to.

As a consequence, if the public link is read-only, you won't be able to edit files even if you had write access via the authenticated share, and the web editors will show `Guest xyz` for your editing session. 


## Microsoft Office and other Windows applications with single-file shares

If you edit a Microsoft Office document via remote access (e.g. via `\\cernbox-smb`, `\\eosproject-smb`, `\\cernbox-drive`, etc.), both on Windows and on Mac OSes (the latter via the `Connect to server...` option in the Finder), most metadata associated to the file, such as the previous versions of the document as well as its permissions if the file was shared as single-file share, are lost each time you save the file.

This is a consequence of how Microsoft Office manipulates files on save operations, coupled with how metadata information such as shares is stored by CERNBox. Similarly, other Windows applications may exhibit the same behavior. Possible workarounds are:

* Use the online Web editors or the Desktop sync client to edit Microsoft Office files.

* If remote native (Windows/Mac) access must be ensured, share the containing folder instead. Note that if remote access is required also for the sharee of your file, you must share the containing folder, otherwise your collaborator won't be able to navigate to the file.


## Git and EOS/CERNBox

If you use `git` on a folder located in `/eos`, e.g. from `lxplus.cern.ch`, chances are that the repository gets corrupted, and you cannot delete files or perform normal git operations. Likewise, if you use `git` on a local folder and then you synchronize it, the remote copy is not usable as a git repository.

In both cases, the recommendation is to refrain from using `git` in an EOS-mounted location, or limit it to read-only (e.g. checkout, pull) operations. Git is extremely powerful but also particularly demanding on the filesystem, and it is best used on locally mounted storage.

