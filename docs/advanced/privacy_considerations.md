---
title: Privacy considerations
---

# Privacy considerations when managing confidential data in CERNBox

CERNBox is [suitable for storing personal data](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003808), including sensitive personal data. CERNBox data and backups are always stored on the CERN premises.

As a CERN user, you have the option to use Microsoft Office to edit files stored in your EOS personal or project spaces. Depending on the licence you are eligible for, you can use:

- [Microsoft Office Online](../web/apps/office_online.md) integrated on the CERNBox web interface. In this case, please note that the document you are editing will be shipped to a Microsoft data centre. The [Record of Processing Operations](https://cern.service-now.com/service-portal?id=privacy_policy&se=CERNBox-Service&notice=CERNBox-v2) provides further details about the data collected by Microsoft.

- Microsoft Office for Desktop, either on a Windows PC managed by you, or through the Windows Terminal Services. 

Please note that in both the above cases telemetry data may be shipped directly from your PC to Microsoft.

If you are concerned about your data privacy, or you are bound by contractual or confidentiality agreements (NDA), *the recommendation is to refrain from using cloud-based services*. In the particular case of sensitive personal data, as defined in [Operational Circular No. 11](https://cds.cern.ch/record/2651311/), the recommendation is to avoid the transfer of Office files containing such data to external entities. 

The [LibreOffice suite](https://www.libreoffice.org/) is available for all major Operating Systems and, as it operates enitirely on premise, is appropriate for processing such Office documents.

Furthermore, in order to keep strict control on who can access confidential or personal data, it is recommended *not* to use public, anonymous links when sharing the data, as by design they can be reshared with no control. Instead, use [authenticated shares](../web/sharing/auth-share.md), and if the recipients are external contractors, ask them to create an [external (or lightweight) account](../web/access.md#accounts-supported).
