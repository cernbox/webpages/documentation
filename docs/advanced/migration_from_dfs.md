# Migration from DFS to CERNBox 

DFS user accounts have been migrated from DFS to CERNBox. Following the migration, your folder tree structure is:

~~~~
Desktop --> C:\Users\<username>\cernbox\WINDOWS\Desktop
Documents --> C:\Users\<username>\cernbox\Documents
Music --> C:\Users\<username>\cernbox\Music
Pictures --> C:\Users\<username>\cernbox\Pictures
Videos --> C:\Users\<username>\cernbox\Videos
Favorites --> C:\Users\<username>\cernbox\WINDOWS\Favorites
Links --> C:\Users\<username>\cernbox\WINDOWS\Links
~~~~

Notes:
Do not move desktop into other folders as your shortcuts will be lost and Windows may not run properly.
If you cannot see your files, don't panic, they are still in CERNBox.
