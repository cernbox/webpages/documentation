# Restoring files from the trash bin

## Restore using the web interface

The restoring of a deleted file from the recycle bin can be done by the owner, for personal spaces, or by one of the administrators, for project spaces. Project administrators are all members of the `cernbox-project-<projectname>-admins` e-group, including the Service Account that owns the project.

If the trash bin contains a large number of files that cannot be conveniently displayed in a web interface, you have to use the command-line interface as described in the next section.

### Personal files

With your Primary Account, open [your trashbin](https://cernbox.cern.ch/files/trash/eos) and browse for the files/folders you want to restore.

### Project files

With your Primary Account, open [your projects in CERNBox](https://cernbox.cern.ch/files/spaces/projects) and click on the `...` menu of the concerned project to find its _Deleted files_ view:

![](../assets/images/project-recycle-bin.png)

In that page, you can browse and restore files as for the standard personal recycle bin.

## Restore using the command-line interface

This operation is restricted to the Account that owns the space (Primary Account for a personal space, Service Account for a project space). To restore a file:

* Login on lxplus with your Primary Account

* If the restore concerns a project space, obtain a kerberos token for the corresponding Service Account

* Set the remote eos address:

  - For personal areas, ``` export EOS_MGM_URL=root://eosuser.cern.ch ```

  - For project spaces, ``` export EOS_MGM_URL=root://eosproject.cern.ch ```

* List the recycle bin files for your account: ``` eos recycle ls ```

* Copy the RESTORE-KEY for your file

* Restore the file: ``` eos recycle restore pxid:<RESTORE-KEY> ```

See example below:

![](../assets/images/project-restore-3.png)

Check that the versions got restored by the eos 'ls -al' command:

``` eos ls -al /eos/project/<initial>/<project-name>/<path-to-.sys.v#-folder> ```

``` e.g. eos ls -al /eos/project/s/swanee/annotation3d.C.nbconvert/.sys.v#.README.docx ```

![](../assets/images/project-restore-2.png)
