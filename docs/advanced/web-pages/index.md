# Web pages in CERNBox

The contents of your website can be hosted on CERNBox (also known as EOSUSER, as this is the back-end storage)

* To host your [personal Website content](personal_website_content.md) in your CERNBox personal space, the steps are:
  1. [Create Web content folder for personal website](personal_website_content.md#create_personal_space)
  2. [Create personal Website (via Web Services)](personal_website_content.md#create-personal-website-via-web-services)
  3. [Migrate Web content already hosted in AFS to EOSUSER](migrate_web_content.md)
* To host your [project Website content](project_website_content.md) in your CERNBox project space, the steps are:
  1. [Put Web contents in www folder](project_website_content.md#create_project_space) 
  2. [Create project Website (via Web Services)](project_website_content.md#create_project_website)
  3. [Migrate Web content already hosted in AFS to EOSUSER](migrate_web_content.md)
  4. [Multiple project Websites](project_website_content.md#multiple_websites) (if this applies to you)
