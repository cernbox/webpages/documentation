# Expose files in Website for public access

It is possible to expose your files and folders in your Website so that the public can access them for download*. 

For some use cases this would also be the best solution for files that might exceed the Web download timeout.

Follow the instructions to [set up your Website](personal_website_content.md).

To summarise, you should have:

* Created your www directory in your CERNbox home folder
* Ensured that the permission ‘can modify’ is __not__ ticked
* For your www folder, the Sharing options must have a:wwweos group in the access list (i.e. choose wwweos from the dropdown menu)
* Put folders/files that you want to be public in your www directory

Now, follow the Webservices instructions [Directory Browsing for EOS sites](https://webeos.docs.cern.ch/manage_site/#enabling-directory-browsing%60)

To summarise, you should have:

* In the www directory, you should have created the file .htaccess with the following line:

    Options +Indexes

Users will now be able to use commands such as 'wget' or 'curl', to download your publicly available files.

If you want to restrict access to your Website, see the Webservices instructions [Access control for EOS web sites](https://webeos.docs.cern.ch/authorization/)

====================================

Example commands of 'wget' and 'curl' are as follows:

To download all files/folders at Website:

* wget -r --reject="index.html*" -e robots=off https://afstoeos.web.cern.ch/afstoeos/

To download one file from the Website:

* wget https://afstoeos.web.cern.ch/afstoeos/red_bus_photo.jpg

To use 'curl' to download one file:

* curl -O https://afstoeos.web.cern.ch/red_bus_photo.jpg

*For heavily-accessed data, do not use this combination (WEBEOS as webservice, serving EOS data) as it will overwhelm the EOSWEB service. An example of this being massively-accessed YUM repositories (i.e more than 10s of machines).

