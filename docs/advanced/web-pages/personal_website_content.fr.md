# Site Web Personnel

Pour héberger du contenu visible sur votre site web personnel, vous devez d'abord créer un dossier spécifique dans votre espace CERNBox.
Ensuite, vous devez faire une demande aux Services Web Centraux du CERN pour permettre l'accès web à ce dossier.

## Créer un dossier de contenu web pour le site web personnel

* Accédez à [CERNBox](https://cernbox.cern.ch) avec un navigateur Web pour voir votre espace EOSUSER.
* Sur la page d'accueil de votre CERNBox, créez un dossier nommé `www` pour héberger votre contenu Web.
Le chemin complet dans EOSUSER est `/eos/user/<initial>/<userID>/www`.
* Partagez votre dossier `www` avec `a:wwweos` (wwweos est le compte de service).

![Créer un site web](../../assets/images/website-03-new.png).
Cliquez sur le bouton "Partager".

* Assurez-vous que l'autorisation est « Invite as Viewer ».

<a name="create-personal-website"></a>
## Créer un site web personnel (via les Services Web)

Consultez les instructions [ici](https://webeos.docs.cern.ch/create_site/)
