# Project Website
Be sure to [request for project space on CERNBox/EOSUSER](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003151) before creating an EOS-based website for your project using a Service Account. 
Using the pre-created www folder, put your website contents there.

## Put web contents in www folder
* Using your own userID, login to [CERNBox](https://cernbox.cern.ch), to access your EOSUSER project space
* Go to ‘Your projects’, and select the project that you want to work on
* Open the project folder, you will see that folder ‘www’ is already created for you

!!! info "Full path of the web folder on EOSUSER (accessible from LXPLUS)"

    `/eos/project/<initial>/<project name>/www` 

Screenshots to walk through the above procedure:

![](../../assets/images/project_web_01.png)

![](../../assets/images/project_web_02.png)

## Create project website (via Web Services)

See the instruction [here](https://webeos.docs.cern.ch/create_site/)

## Multiple project websites
Some projects have more than one website that are managed by the SAME Service Account. For this use case
* The content of the websites should be hosted in sub-folders, under 'www'
* For each website, create `/eos/project/<initial>/<projectname>/www/<website'n' folder>`, where `website'n' folder` will hold the contents for your `<website1>`, `<website2>`, etc.. You must create the folders BEFORE creating the website
* Go to [Web Services Portal](https://webservices-portal.web.cern.ch/webeos) to give the details of for each of the websites: 
  * select `Site category=Official webpage`
  * Enter site name and site description
  * For each website enter the specific path `/eos/project/<initial>/<project name>/www/<website'n' folder>` that hosts the website contents

![](../../assets/images/portal-webeos-creation-form.png)

