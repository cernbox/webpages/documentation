# Personal Website

To host material to be visible on your personal website you first have to create a specific folder in your CERNBox space.
After that, you have to make a request to CERN Central Web Services to allow web access to it.

## Create web content folder for personal website

* Access [CERNBox](https://cernbox.cern.ch) with a Web browser to see your EOSUSER space.
* In your CERNBox homepage, create a folder named `www`, to host your Web content.
The full path in EOSUSER is `/eos/user/<initial>/<userID>/www`
* Share your folder `www` with `a:wwweos` (wwweos is the service account).

![Create a website](../../assets/images/website-03-new.png).
Click the "Share" button.

* Ensure that the permission is ‘Invite as Viewer’.


<a name="create-personal-website"></a>
## Create personal Website (via Web Services) 

See the instruction [here](https://webeos.docs.cern.ch/create_site/)
