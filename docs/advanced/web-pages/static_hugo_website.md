# Deploy a static Hugo website on EOS with GitLab CI

This guide assumes you've already [requested project space on CERNBox/EOSUSER](https://cern.service-now.com/service-portal?id=kb_article&n=KB0003151) using a Service Account and [created a project website (via Web Services)](https://webservices-portal.web.cern.ch/webeos). Refer to [these docs](project_website_content.md) if you need help with those steps.

## Create a Hugo project in GitLab

Hugo is a popular, fast and flexible static site generator written in Go. It takes a directory with content written in markdown and templates and renders them into a full HTML website. Check its comprehensive [quick start guide](https://gohugo.io/getting-started/quick-start/) to set up your first project and push it to [GitLab](https://gitlab.cern.ch).

> _Note_: Once created, you'll need to adjust your project's `baseURL` in Hugo's `config.toml` to point to your project's website path. For instance, if your site's name is `test-project` (`http://test-project.web.cern.ch/test-project/`) you'll need to set `baseURL = "/test-project"`.

From there, it's easy to configure an automated pipeline in the project to perform these two actions for us:

1. Generate the static files (HTML, image assets...) using the `hugo` cli.
2. Deploy the artifacts on the project's `www` folder.

## Configure GitLab CI to deploy your site to EOS

We will rely on the [`deploy-eos`](https://gitlab.cern.ch/gitlabci-examples/deploy_eos) scripts for the heavy load in terms of authentication and file upload. Quoting its README:

> In the GitLab project settings, you must define the following set of [secure variables](https://gitlab.cern.ch/help/ci/variables/README.md#user-defined-variables-secure-variables) to
provide the `deploy-eos` script with the destination EOS folder and credentials needed to write to that folder:

![](../../assets/images/gitlabci_settings.png)

| Variable               | Content                                                               |
|------------------------|-----------------------------------------------------------------------|
| `EOS_PATH`             | Path of the EOS folder - e.g. `/eos/project/<initial>/<project>/www/` |
| `EOS_ACCOUNT_USERNAME` | Service account for `EOS_PATH`                                        |
| `EOS_ACCOUNT_PASSWORD` | The Service account password                                          |

![](../../assets/images/gitlabci_variables.png)

### Add the `.gitlab-ci.yml` file to your project

Once you have a project structure and all the EOS variables set. You just need to create and push a `.gitlab-ci.yml` file on the root of your repository, describing the two stages of the deployment pipeline:

```yaml
variables:
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - build
  - deploy

compilation:
  stage: build
  image: registry.gitlab.com/pages/hugo/hugo_extended:latest
  before_script:
    - apk add git npm
    - npm install
  script:
    - hugo
  artifacts:
    paths:
      # Upload to GitLab the folder where the HTML output has been generated
      - public
    expire_in: 1 hour

deployment:
  stage: deploy
  image: gitlab-registry.cern.ch/ci-tools/ci-web-deployer:latest
  script:
  # Copy the generated content to the folder in EOS_PATH
    - deploy-eos
  before_script: []
  after_script: []
```

> _Note_: Depending on your theme and its dependencies, you might need to install and run `npm` as part of the `before_script` on your `build` job as it's done above, remove those lines if you don't need them.

> _Note_: You can also limit when the deploy happens (i.e. which branch triggers a re-deployment) by using the [`only`/`except`](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic) GitLab CI parameters. Or even set a [`manual`](https://docs.gitlab.com/ee/ci/yaml/#whenmanual) trigger for the deploy.

Once both jobs on the pipeline are done, you should be good to go. Head over your project's website to check your new site.
