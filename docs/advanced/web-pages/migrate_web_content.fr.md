## Comment migrer un site Web de AFS à EOSUSER

* Depuis une machine connecté à AFS (e.g. lxplus):

        % eos cp --recursive <source> <destination>

    e.g.  % eos cp --recursive /afs/cern.ch/user/a/abchan/www/ root://eosuser.cern.ch//eos/user/a/abchan/www/


* Depuis une machine connectée à la fois à AFS et à EOS (e.g. lxplus-cb6):

        % rsync --av <source> <destination>

    e.g.  % rsync -av /afs/cern.ch/user/a/abchan/www/ /eos/user/a/abchan/www/


