# Sites Web dans un espace Projet

* S'assurer d'avoir demander une espace de projet dans CERNBox, se reporter au chapitre ***[Espace projet dans CERNBox](../../web/projects/requesting_for_project_space.md)*** de ce manuel utilisateur

## Mettre le contenu dans le répertoire www
* En utilant un compte primaire, se connecter à [CERNBox](https://cernbox.cern.ch), pour accéder votre espace de projet sur EOSUSER.
* Aller à ***Your Projects***, et sélectionner le projet sur lequel vous coulez travailler.
* Ouvrir le répertoire de projet, où est déjà présent le dossier `www`.

!!! info "Chemin du dossier web sur EOSUSER (accessible depuis LXPLUS"

    `eos/project/<initial>/<nom de projet>/www`

![](../../assets/images/project_web_01.png)

![](../../assets/images/project_web_02.png)

## Créer le website du projet (via Web Services)
Aller [ici](https://cern.ch/webservices/Services/CreateNewSite/) 
* Sélectionner `Site category=Official website`
* Sélectionner `Site type=EOS folder`
* Chemin : `/eos/project/<initial>/<nom de projet>/www`

## Projet avec plusieurs sites web
Certains projets ont plus d'un unique site web gérer par le même compte de service. Pour cela :
* Le contenu des sites web doivent être hébergé sous différents sous-dossier de `www`
* Pour chaque sites web, céer `eos/project/<initial>/<nom de projet>/www/<site web n>`, où `<site web n>` contiendra le contenu des différents sites web. Ces sous-dossier doivent être créé AVANT de créer le website
* Aller à [https://cern.ch/webservices/Services/CreateNewSite/](https://cern.ch/webservices/Services/CreateNewSite/) pour donner des détails sur les sites :
  * Sélectionner `Site category=Official website`
  * Sélectionner `Site type=EOS folder`
  * Chemin : `/eos/project/<initial>/<nom de projet>/www/<site web n>`

![](../../assets/images/project_web_multiple_01.png)
