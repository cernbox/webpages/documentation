# Migrate WEB files already hosted in AFS to EOSUSER

On a machine that has both AFS and EOSUSER (e.g. lxplus):

`$ rsync -av <source> <destination>`

*   User space: e.g. 
`$ rsync -av /afs/cern.ch/user/a/abchan/web_content_folder/* /eos/user/a/abchan/www/`
*   Project space: e.g. 
`$ rsync -av /afs/cern.ch/user/a/abchan/web_content_folder/* /eos/project/c/cboxeosuser-demo/www/`


Copying files from your own CERNBox/EOSUSER folders to the project space (e.g. on lxplus):


`$ rsync -av <eosuser source> <eosproject destination>`


e.g. `rsync -av /eos/user/a/abchan/web_contect_folder/* /eos/project/c/cboxeosuser-demo/www/`

