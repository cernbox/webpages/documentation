# Pages Web dans CERNBox 

Vous pouvez héberger des sites Web dans votre espace personnel CERNBox et les rendre accessibles depuis l'extérieur à condition de suivre la procédure décrite ci-dessous.

* Pour héberger du [contenu web personel](personal_website_content.md) dans votre CERNBox, les étapes sont :
  1. [Créer un repertoire web pour un site personnel](personal_website_content.md#create_personal_space)
  2. [Faire une demande de site web via le web service](personal_website_content.md#create-personal-website-via-web-services)
  3. [Migrer du contenu web de AFS vers EOS](migrate_web_content.md)
* Pour héberger du [contenu web pour un projet](project_website_content.md)
  1. [Mettre le contenu web dans le répertoire www](project_website_content.md#create_project_space)
  2. [Créer un site web du projet](project_website_content.md#create_project_website)
  3. [Migrer du contenu web de AFS vers EOS](migrate_web_content.md)
  4. [Gerer des projets avec plusieurs sites web](project_website_content.md#multiple_websites)
