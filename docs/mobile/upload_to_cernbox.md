# Upload to CERNBox 

In order to put files that are on your phone into CERNBox, find the file, select (and hold). This menu gets displayed:

![](../assets/images/android-phone-5.png)

To share the file to your CERNBox, select **_cernbox_**:

![](../assets/images/android-phone-6.png)

Choose the CERNBox folder that you want to upload the file into: 

![](../assets/images/android-phone-7.png)

On this phone the menu displayed on selecting a folder did not show the option **_Share_**.
