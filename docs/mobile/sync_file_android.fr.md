# Synchroniser des fichiers en utilsant l'application Android

Sur ce télephone mobile, les fichiers sont pas synchronisés en continus. Par contre, recharger le fichier synchronise celui ci.

Actuellement, ***Keep the file up to date*** indique les fichiers en favoris sur l'appareil.

Afin de mettre vos fichiers en favoris, trouver le, sélectionner le (et maintenir) afin d'afficher le menu et sélectionner ***Détails***

![](../assets/images/android-phone-3.png)

Cocher la case ***Keep the file up to date***

![](../assets/images/android-phone-4.png)

Maintenant, une étoile jaune est affiché à côté du fichier pour montrer qu'il est dans vos favoris.

![](../assets/images/android-phone-8.png)

La synchronisation, dans ce cas, peut seulement avoir lieu quand vous êtes en ligne. Si vous êtes hors-ligne vous avez toujours accès à la copie sur l'appareil mais il ne sera pas synchronisé jusqu'au moment ou vous serez à nouveau en ligne.
