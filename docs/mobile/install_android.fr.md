# CERNBox Android App

Comme pour toutes les applications sur Android, vous pouvez télécharger l'application CERNBox via:

[![Play Store](../assets/images/android-icon-1.png)](https://play.google.com/store/apps/details?id=ch.cern.cernbox)

Les illustrations qui suivent ont été réalisées sur un téléphone Samsung Galaxy S5.

Sélectionner l'application CERNBox via Google Play:

![](../assets/images/android_1.png)


![](../assets/images/android_2.png)


Et cliquer sur le bouton ***INSTALLER***. Lorsque l'installation est terminée, cliquer sur ***OUVRIR***, la liste de vous fichiers/dossiers dans CERNBox s'affiche:

![](../assets/images/android_3.png)
