# CERNBox for mobile devices

The recommended method for accessing your CERNBox with Android or Apple iOS devices is by using the dedicated CERNBox mobile apps that you can download via 

[![Play Store](../assets/images/android-icon-1.png)](https://play.google.com/store/apps/details?id=ch.cern.cernbox)

or

[![App store](../assets/images/app-store-icon-1.png)](https://itunes.apple.com/en/app/cernbox/id1082672644)

These clients come with a default supported configuration.
