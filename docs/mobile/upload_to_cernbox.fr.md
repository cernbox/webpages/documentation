# Upload vers CERNBox

Afin de mettre des fichiers dans CERNBox, trouver le fichier, faites un appui long dessus et le menu suivant s'affichera :

![]..(/assets/images/android-phone-5.png)

Afin de partager ce fichier sur CERNBox, selectionner ***cernbox***

![](../assets/images/android-phone-6.png)

Choisir le répertoire CERNBox où le fichier va être :

![](../assets/images/android-phone-7.png)
