# Install Android App
As for any application on Android, you can install CERNBox via 

[![Play Store](../assets/images/android-icon-1.png)](https://play.google.com/store/apps/details?id=ch.cern.cernbox)

The following illustrations are made on a Samsung Galaxy Ace phone. Your Android phone app will have similar features and displays.

Via Google Play search for and then select CERNBox:

![](../assets/images/android-phone-0.png) 

This goes to:

![](../assets/images/android-phone-0.5.png)

Click on _**INSTALL**_. 

Once installed, click on **_OPEN_** to start the app. You will be see the list of folders in your CERNBox Files page: 

![](../assets/images/android-phone-2.png)



