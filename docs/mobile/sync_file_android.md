# Sync file using Android app

On this mobile phone (Samsung Galaxy Ace) the files are not kept continually synchronised. However, reloading the file synchronises the files.

Currently, the **_Keep file up to date_** indicates favourite files on the mobile phone.

To show your favourite CERNBox file, find it, select (and hold) to display the menu and select **_Details_**:

![](../assets/images/android-phone-3.png)

Tick the box **_Keep file up to date_**:

![](../assets/images/android-phone-4.png)

You will have a yellow star next to the file to indicate that this file is a favourite file:

![](../assets/images/android-phone-8.png)

The synchronisation, in this case reloading, can only take place when you are ONLINE. If you are not online, you can still access the local copy that you  previously downloaded on your phone, but this cannot be kept synchronised until you are online again.



