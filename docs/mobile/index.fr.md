# CERNBox pour équipements mobiles

La méthode recommandée pour garder vos équipements mobiles Android et/ou iOS synchronisés avec CERNBox est d'installer les applications mobiles CERNBox depuis Google 

[![Play Store](../assets/images/android-icon-1.png)](https://play.google.com/store/apps/details?id=ch.cern.cernbox)

ou

[![App store](../assets/images/app-store-icon-1.png)](https://itunes.apple.com/en/app/cernbox/id1082672644)

Ces applications disposent d'une configuration par défaut.
