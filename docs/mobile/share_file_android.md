# Share file using Android app

Using the Android app, the way to share your CERNBox file (or folder) to someone else is via a link. 

* In your CERNBox, find the file (or folder), select it like that :

![select](../assets/images/android_share_select_en.png)

* Then click on the share button ![]../assets/images/share_icon.png) wich will bring you to this menu :

![share menu](../assets/images/android_share_option_en.png)

* Select ***SHARE LINK***, it will then display options for the share link :

![share link options](../assets/images/android_link_share_option_en.png)

* Then you can click on ***GET LINK***, which lead to a list of means to send the link :

![Lists of medium](../assets/images/android_link_share_choice_en.png)
