# Partager un fichier et/ou un répertoire en utilisant l'application Android

En utilisant l'application Android, la seule façon de partager un lien est via le partage par lien ;

* Dans l'application CERNBox, trouver le fichier ou le dossier à partager et sélectionner le :

![sélection](../assets/images/android_share_select_fr.png)

* Ensuite sélectionner le bouton de partage ![](../assets/images/share_icon.png) ce qui vous ammène à ce menu :

![menu de partage](../assets/images/android_share_option_fr.png)

* Sélectionner ***PARTAGER PAR LIEN PUBLIC***, ce qui affiche certaines option pour le partage par lien :

![options du partage par lien](../assets/images/android_link_share_option_fr.png)

* Ensuite, cliquer sur ***OBTENIR LE LIEN***, qui vous ammène vers une liste de moyen pour envoyer le lien :

![listes de moyen](../assets/images/android_link_share_choices_fr.png) 
