# API access to data in CERNBox

!!! Warning
    This access method is reserved to developers and integrators, who need to interact with CERNBox and/or the underlying EOS storage from another service. End users should use the available regular [access methods](../desktop/other-access-methods/desktop-mobile-access.md).

If you are a developer and your workflow requires programmatic access to CERNBox and/or EOS, you can follow these guidelines to automate specific actions.

Note that for large volumes of data, it is recommended to directly interact with [EOS](https://eos-web.web.cern.ch), possibly using HTTP Third-Party Copy (TPC).

In the following, `wget` and `curl` are used to illustrate how to use the API.

## Public links

Files or folders that have been shared via public links can be anonymously downloaded as follows:

```bash
curl https://cernbox.cern.ch/remote.php/dav/public-files/<public-link-hash>/<relative-path-to-file>
```

If you have created a public link on a folder with _edit_ or _uploader_ rights, then the following allows you to anonymously upload content to that folder:

```bash
curl -T <some-file> -X PUT https://cernbox.cern.ch/remote.php/dav/public-files/<public-link-hash>/<target-filename>
```

Note that for _uploader_ links, the target file gets an extra hash appended to the name, to prevent collisions as the user does not have rights to list or overwrite existing content (see also [sharing](../web/sharing/public-share.md)).


## Protected shares

Files or folders under protected shares can be accessed using HTTP Basic authentication at the `api.cernbox.cern.ch` endpoint. This endpoint is accessible withint the CERN network only. For example:

```bash
wget --user <account> --password <password> https://api.cernbox.cern.ch/remote.php/dav/files/<account>/eos/<full-path-to-target>
```

Note that such a full path MUST contain `<account>` above the EOS path.

Similarly, it is possible to upload content to protected shares:

```bash
curl -T <some-file> -u <account>:<password> -X PUT https://api.cernbox.cern.ch/remote.php/dav/files/<account>/eos/<full-path-to-target>
```


## Share API

CERNBox implements the [ownCloud Share API](https://doc.owncloud.com/server/next/developer_manual/core/apis/ocs-share-api.html). This API is authenticated, and can be accessed at the `https://api.cernbox.cern.ch` endpoint via Basic Auth within the CERN network.
