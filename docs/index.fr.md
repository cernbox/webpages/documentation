---
template: homepage.html
---


![CERNBox logo](assets/images/logo-full.png){ width="160" height="120" style="display: block; margin: 0 auto" }

[CERNBox](https://cernbox.cern.ch/) fournit un stockage de données dans le cloud à tous les utilisateurs du CERN. Vous pouvez stocker vos données, les partager et les synchroniser sur tous les appareils - smartphones, tablettes, ordinateurs portables, ordinateurs de bureau, etc. ! Les données sont accessibles depuis n'importe quel navigateur Web ou explorateur de fichiers, et vous décidez quelles données vous souhaitez partager avec d'autres personnes ou groupes de collaborateurs. 

Les données peuvent être synchronisées avec vos appareils (Windows, Mac, Linux, iOs et Android) avec l'application CERNBox (alias. le client de synchronisation). Vous pouvez, par exemple, conserver vos fichiers sur CERNBox et ne synchroniser que ceux que vous utilisez souvent, ou vous pouvez synchroniser une partie des données intéressantes de votre expérience sur votre ordinateur portable pour les tests. CERNBox s'intègre également à certaines applications pour permettre l'édition collaborative de cahiers interactifs pour l'analyse physique, ainsi que d'autres produits.

En savoir plus sur l'utilisation de CERNBox via

  <div class="columns">
    <a href="web" class="box">
        <div>
            <div class="left">
                <img src="/assets/icons/web.svg">
            </div>
            <div class="right">
                <h1>Web</h1>
                <p>Nouvelle interface utilisateur, partage, applications, projets, ...
            </div>
        </div>
    </a>
    <a href="desktop/sync-client/" class="box">
        <div>
            <div class="left">
                <img src="/assets/icons/desktop.svg">
            </div>
            <div class="right">
                <h1>Desktop</h1>
                <p>Client de sync pour votre ordinateur, différentes méthodes d'accès, ...
            </div>
        </div>
    </a>
    <a href="mobile" class="box">
        <div>
            <div class="left">
                <img src="/assets/icons/cellphone.svg">
            </div>
            <div class="right">
                <h1>Mobile</h1>
                <p>Installer et utiliser depuis Android et iOS, ...
            </div>
        </div>
    </a>
  </div>

!!! info "Assistance"
    Vous n'avez pas trouvé la réponse à votre question dans la documentation ? Vous avez des commentaires ou souhaitez signaler un incident ? Créer un [SNOW ticket.](https://cern.service-now.com/service-portal?id=service_element&name=CERNBox-Service)


!!! note "Améliorer cette documentation"
    Vous pensez que quelque chose peut être amélioré dans cette documentation ? Vous êtes invités à [contribuer](https://gitlab.cern.ch/cernbox/new-cernbox-guide)!

