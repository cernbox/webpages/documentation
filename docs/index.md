---
template: homepage.html
---


![CERNBox logo](assets/images/logo-full.png){ width="160" height="120" style="display: block; margin: 0 auto" }

[CERNBox](https://cernbox.cern.ch/) provides cloud data storage to all CERN users. You can store your data, share it and synchronise it across devices - smartphones, tablets, laptops, desktops, the lot! The data can be accessed from any Web browser or file explorer, and you decide which data you want to share with other individuals or groups of collaborators. 

Data can be synchronised to your devices (Windows, Mac, Linux, iOs and Android) with the CERNBox application (aka. the sync client). You can, for example, keep your files on CERNBox and synchronise only the ones you use often, or you can synchronise part of the interesting data from your experiment on to your laptop for testing. CERNBox also integrates with some applications to allow collaborative editing of interactive notebooks for Physics analysis, as well as other products.

Learn more how to use CERNBox via:

  <div class="columns">
    <a href="web" class="box">
        <div>
            <div class="left">
                <img src="assets/icons/web.svg">
            </div>
            <div class="right">
                <h1>Web</h1>
                <p>New web UI, sharing, applications, projects, ...
            </div>
        </div>
    </a>
    <a href="desktop/sync-client/" class="box">
        <div>
            <div class="left">
                <img src="assets/icons/desktop.svg">
            </div>
            <div class="right">
                <h1>Desktop</h1>
                <p>Sync client for your computer, different access methods, ...
            </div>
        </div>
    </a>
    <a href="mobile" class="box">
        <div>
            <div class="left">
                <img src="assets/icons/cellphone.svg">
            </div>
            <div class="right">
                <h1>Mobile</h1>
                <p>Install and use from Android and iOS, ...
            </div>
        </div>
    </a>
  </div>

!!! info "Support"
    You didn't find the answer to your question in the documentation? You have feedback or want to report an incident? Create a [SNOW ticket.](https://cern.service-now.com/service-portal?id=service_element&name=CERNBox-Service)


!!! note "Improve this documentation"
    You think something can be improved in this documentation? You are welcomed to [contribute](https://gitlab.cern.ch/cernbox/new-cernbox-guide)!