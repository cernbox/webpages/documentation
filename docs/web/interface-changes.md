# The Interface


## Light and dark mode

On the right side of the topbar, you can now turn on the new dark mode for the interface.

![Light mode](../assets/screenshots/light-mode.png)
![Dark mode](../assets/screenshots/dark-mode.png)

## Files list interactions


The context menu is now also accessible via right click in addition to the :material-dots-vertical: menu. 
This allows users to perform typical actions even faster, for example, downloading documents or opening documents in the apps.

[Learn more about managing files](managing-files.md)

## Easier document sharing

The new Web UI makes sharing content easier. Quick action "Copy Quicklink" enables you to copy the resource public link called "Quicklink". It is defaultly the first of the public links.

It’s just as easy to invite users as viewers or editors to view or collaborate on documents by clicking "Share" quick action. In addition you can send an email notification for selected sharees on clicking "Share" by checking the corresponding checkbox. You can notify an existing sharee retroactively by selecting the option in options menu of the corresponding sharee.

![Sharing](../assets/screenshots/sharing2.png)

## Overview of shares

We improved the overview of your shares. You can apply different grouping settings of shares list for an easier exploration. In "Shared with me" section you can hide shares as well as unhide them from a hidden shares list.
![Shares overview](../assets/screenshots/shares-overview.png)

[Learn more about sharing](sharing/index.md)

## "New Features" Guide

Get overview of new features by clicking on "New Features" in the topbar.


## Improved accessibility

The new accessibility features have been added to the user interface. The new Web UI can be controlled via keyboard, offers improved color contrast for better readability, and supports navigation and operation via screen reader. 
