# Files management

## View modes for resources

You can change the view modes of your resources in the top right corner of the files area. 

### Table view

The table view lists your files in a table. You can sort the files by clicking the corresponding columns' names.
![Table view](../assets/screenshots/table_view.png)

### Condensed table view
The condensed table view shows the files in a more compact way so that you can see more files at once.

### Tiles view
With the tiles view the files are shown as tiles. You can sort the files by selecting an option from "Sort by". There is also a possibility to change tiles sizes in view settings.
![Tiles view with settings](../assets/screenshots/tiles_view2.png)


## Adding files

To add files, you can use the `Upload` or `New` buttons. To see what file extensions are viewable/editable by the integrated applications, please check the [applications](apps/index.md#online-viewingediting-capabilities) page.

![Create a new file](../assets/screenshots/new-file-menu.png)

It's also possible to drag-and-drop a file/folder into the center of the page.

## Organization

To move files around, it's possible to drag-and-drop files into subdirectories, as before, but now there's also a `Move` and `Copy` options.

File actions are accessible via right click menus or :material-dots-vertical: menus. Selected files will have extra options in the right sidebar, opened by clicking in `Details` or the sidebar button (:octicons-sidebar-expand-16:).

To show or hide hidden files (files whose name start with a dot `.`), please set the option in the files view settings menu (:material-cog:).

## Favorites

Now it's possible to reliably favorite a file/folder from any place, creating a shortcut available from the `Favorites` page.
