# Gérer vortre projet

Si votre requête est acceptée, un nouveau répertoire sera créé dans EOS (`/eos/project/p/projectname`) contenant les sous-dossiers `www` et `public`. Ce chemin est accessible sur LXPLUS.

Par example, pour le projet 'lattice', le répertoire sera `/eos/project/l/lattice`, et le projet aura les dossiers suivants créés automatiquement
* `/eos/project/l/lattice/www/` (répertoire pour un héberger un site web type EOS)
* `/eos/project/l/lattice/public/` (répertoire public accessible par tout les utilisateurs du CERN)

Ce répertoire sera alors visible dans CERNBox aux membres du projet (utilisateurs qui sont dans les egroups `readers` ou `writers`). Les utilisateurs le retrouve en cliquant sur ***All projects*** ou mieux, sur  ***Your projects***  pour y accèder.

En pratique, le service CERNBox/EOS aura créer 3 egroups :

    * `cernbox_projectname_admins`: administrateurs du projet 
    * `cernbox_projectname_writers`: les personnes ayant accès en écriture au projet, cela inclus le(s) membre(s) du groupe `admin`
    * `cernbox_project_readers`: les personnes ayant uniquement un accès en lecture aux données du projet.
     
Pour ajouter de nouveaux administrateurs au groupe `cernbox-project-<nomdeprojet>-admins`, le propriétaire du compte de service doit se connecter (avec son compte primaire) à [https://e-groups.cern.ch](https://e-groups.cern.ch) et ajouter des membres à cet egroup.
Les membres de cet egroup peuvent donner droits de lectures et écritures en ajoutant des membres au egroupes `writers` et `readers`.

> **[warning] Temps d'attente (pour la propagation) après l'ajout de membres au egroupes**
>
> Le temps d'attente recommendé est de 40 minutes, mais il est arrivé que cela prenne malheureusement plus longtemps.

&nbsp;

> **[warning] L'option ***Partage*** fournit par CERNBox n'est pas activé pour le répertoire de projet**
>
> Tout les répertoires du dossier peuvent être partagés par lien ou partage authentifié, mais **seulement** le compte de service peut faire ça (les membres de `admin` ne peuvent pas faire de partage pour l'instant)
 
&nbsp;

> **[info] Rappel : il est impossible de partager un dossier si un de ces dossiers plus bas dans l'arbre a déjà été partagé**
>
> Voir [ici](https://cern.service-now.com/service-portal/article.do?n=KB0004537)

Quand le compte de service se connecte à CERNBox, la page d'accueil affiche les fichiers/répertoires du projet.


## Partage des ressources au sein du projet

Il est possible de partager touts les dossiers dans un projet. Le processus est le même que celui décrit pour [shares](../sharing/index.md).

Cette option est conçue comme un moyen de remplacer les autorisations d'accès existantes pour un utilisateur ou un groupe, et donc elle a toujours priorité sur eux. Si vous avez besoin de "dé-refuser" un accès refusé, c'est-à-dire de redonner accès à un sous-dossier d'un dossier qui a été refusé, vous trouverez ici quelques suggestions pour implémenter céla pour deux scénarios courants :

1. L'utilisateur `U` est membre du groupe `cernbox-project-myproject-readers`, donc l'accès est accordé à l'ensemble du projet `/eos/project/m/myproject`. Ensuite, un `deny` est défini sur `/eos/project/m/myproject/not-for-U`, et `/eos/project/m/myproject/not-for-U/public` devrait au contraire être accessible à `U`. Envisagez de supprimer `U` du groupe -readers et partagez simplement `/eos/project/m/myproject/not-for-U/public` avec `U`. Vous pouvez également déplacer le dossier `not-for-U/public` plus haut dans l'arborescence des dossiers, par ex. au sommet.

2. L'utilisateur `U` a reçu un accès à `/eos/project/m/myproject/something-for-U`, et a été refusé dans `/eos/project/m/myproject/something-for-U/top-secret`. Ensuite, un dossier `/eos/project/m/myproject/something-for-U/top-secret/to-be-released` doit être partagé à nouveau avec `U`. Envisagez de déplacer ce dernier dossier d'un ou plusieurs niveaux vers le haut.

En général, le modèle d'autorisation CERNBox est conçu en sorte qu'il fonctionne mieux si vous ouvrez les autorisations en descendant dans la hiérarchie des dossiers, à l'exception des "deny". D'autres systèmes, notamment DFS, permettent plutôt de commencer avec des autorisations maximales, et de restreindre au fur et à mesure que vous descendez dans la hiérarchie des dossiers. Si vous avez un cas d'utilisation où vous ne pouvez pas représenter les autorisations requises à l'aide de ce modèle, veuillez nous contacter.
