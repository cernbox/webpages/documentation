# Projets et autres sources de données

## Projets

Pour avoir un aperçu des projets auxquels le compte connecté a accès, il est possible de les lister en cliquant sur `Projets` dans le menu de gauche.

Maintenant, il est également possible de voir et de récupérer des fichiers de projets en faisant un clic droit sur un projet puis sur "Ouvrir la corbeille".

![Corbeille du projet](../../assets/screenshots/projects-trashbin.png)

Les administrateurs de projet peuvent désormais voir les partages associés à un fichier/dossier et pourront les modifier. Tout partage créé par un administrateur sera associé au compte de service propriétaire du projet.

## Autres sources de données

D'autres sources de données seront également ajoutées à CERNBox. Accès aux autres instances EOS ou aux répertoires d'accueil HPC, par exemple.