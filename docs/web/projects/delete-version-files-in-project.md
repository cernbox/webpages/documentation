# Delete version files

In some use cases, projects create many version files such that they consume the project's allowance of space. The commands below show you how to delete version files, but **WARNING** please be aware that you will lose one of the three ways to recover your files (the other two being the recycle bin and our backups).

The deletion of version files can only be done by the Service Account of the project.

To delete version files in your project from the 'base-folder' down: 

 *  Login on lxplus with the **Service Account** for the project
 *  To list version folders:
  
```eos root://eosproject.cern.ch ls -al <path-to-folder>```
    
e.g. eos root://eosproject.cern.ch ls -al /eos/project/a/afstoeos-bc-test/testingWorms/
    

A version folder has:
   
``` .sys.v#.<file-name>```
    
``` e.g. .sys.v#.worms.xlsx```







To list the version files in a version folder:

```eos root://eosproject.cern.ch ls -al <path-to-folder>.sys.v#.<file-name> ```
    
``` e.g. eos root://eosproject.cern.ch ls -al /eos/project/a/afstoeos-bc-test/testingWorms/.sys.v#.worms.xlsx  ```  

    
![](../../assets/images/versions-delete-10-1.png)
 *  Determine your 'base-path', which is the folder AT and BELOW which all version files will be deleted. 
    In the example used here, the 'base-path' is actually the path to the project itself. You can decide to use a folder lower down your folder tree.
    The command to delete all version files and folders AT and BELOW your 'base-path' is:
    
  
```   eos root://eosproject.cern.ch find -f /eos/project/<initial>/<project-name>/<base-path> | grep .sys.v | sed -r 's|/[^/]+$||' | sort | uniq | awk '{printf "eos root://eosproject.cern.ch rm -r %s\n", $0}' | sh -x ```

    e.g. eos root://eosproject.cern.ch find -f /eos/project/a/afstoeos-bc-test/ | grep .sys.v | sed -r 's|/[^/]+$||' | sort | uniq | awk '{printf "eos root://eosproject.cern.ch rm -r %s\n", $0}' | sh -x ```

    
![](../../assets/images/versions-delete-20-1.png)
 
