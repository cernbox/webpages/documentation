# Manage your project

After your request has been approved, an EOS directory,`/eos/project/<initial>/<projectname>`, will be created. The project will contain two sub-folders:
   - www
   - public 

Three (3) CERN e-groups to be **used to control access to the ENTIRE project space** will be created:  

   * `cernbox-project-<projectname>-admins`  (members can add new administrators. Administrators can add users to `-writers` and `-readers` egroups) 
   *  `cernbox-project-<projectname>-writers` (members can read, write, delete in the project space. The `-admins` e-group is already included in the `-writers` egroup)
   * `cernbox-project-<projectname>-readers` (members can only read the files in the project space)
     
To add other project space administrators to `cernbox-project-<projectname>-admins`, the Service Account holder has to login (using his/her own primary account) to https://e-groups.cern.ch and add other project space administrators to this egroup.
Any member of `cernbox-project-<projectname>-admins` can give read/write access to a user (or an e-group) to the project. He/she needs to login (using his/her own primary account) into https://e-groups.cern.ch, and add the user (or e-group) to either the `-readers` or `-writers` e-groups.

!!! info "Waiting time (for propagation) after adding members to the project access e-groups"

    This process takes a relatively long time (hours if not days), as it depends on a number of synchronization processes upstream of CERNBox. Check Service Now for more details.

The project space can be accessed both via CERNBox (Web browser or client) and on lxplus. 

## Going to the project on Lxplus
For example on Lxplus, for a project called 'lattice' the project directory will be `/eos/project/l/lattice`, and the project will have the following pre-created folders:
   - `/eos/project/l/lattice/www/`    (folder for hosting contents for an EOS-site-type website)
   - `/eos/project/l/lattice/public/` (public folder for all CERN authenticated users)

## Going to the project in CERNBox
All members of the CERNBox project see the contents of the project by logging into https://cernbox.cern.ch and going to the ***Projects*** list (on left-hand side), and then clicking on the project name.

## Sharing the resources inside the project

It's possible to share any of the project folders. The process is the same as described for [shares](../sharing/index.md).

In addition, project admins have the option to restrict access to folders inside a project area, targeting users or groups that already had access: this can be achieved by creating an [authenticated share](../sharing/auth-share.md) where the permission is set to "Deny access". This option is intended as a way to override existing access permissions for a user or group, and as such it always takes precedence over them. If you find yourself needing to "un-deny" a denied access, i.e. giving access again to a subfolder of a folder that was denied, here you find some suggestions about how you can implement that for two common scenarios:

1. User `U` is member of the `cernbox-project-myproject-readers` e-group, therefore access is granted to the whole project `/eos/project/m/myproject`. Then, a `deny` is set to `/eos/project/m/myproject/not-for-U`, and `/eos/project/m/myproject/not-for-U/public` should instead be accessible to `U`. Consider removing `U` from the -readers e-group, and just share `/eos/project/m/myproject/not-for-U/public` with `U`. Alternatively, move the `not-for-U/public` folder higher in the folder tree, e.g. at the top.

2. User `U` was given access via share to `/eos/project/m/myproject/something-for-U`, and denied in `/eos/project/m/myproject/something-for-U/top-secret`. Then, a folder `/eos/project/m/myproject/something-for-U/top-secret/to-be-released` is to be shared again with `U`. Consider moving the latter folder one or more levels up.

In general, the CERNBox permission model is designed such that it works best when opening up the permissions the more you go down the folders' hierarchy, with the exception of denials. Other systems, notably DFS, allow instead to start with maximum permissions, and restrict as you go down the folders' hierarchy. If you have a use-case where you cannot map the required permissions using this model, please get in contact with us.
