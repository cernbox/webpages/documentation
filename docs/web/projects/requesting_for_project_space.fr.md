# Demande d'espace projet

Espace de projet pour les expériences/projets :

Si votre expérience ou projet le nécessite, et **NE DISPOSE PAS DÉJÀ D'ESPACE sur EOS**, vous pouvez faire une demande d'espace : remplissez le [Demander un espace projet sur EOS/CERNBOX](https://cern.service-now.com /service-portal?id=sc_cat_item&name=request-storage-space&se=CERNBox-Service) dans Service-Now.

Il vous sera demandé de fournir les informations suivantes dans un premier temps :

1. Quel est le nom de l'expérience/du projet ? Il sera utilisé pour le nom du répertoire.

2. Une brève description de l'expérience/du projet.

3. Espace nécessaire (par défaut 1 To).

4. Veuillez fournir un [compte de service] (https://account.cern.ch/account/Management/NewAccount.aspx). Toutes les données resteront attachées à ce compte ; le compte peut être transféré à une autre personne, si nécessaire (par exemple, quelqu'un quittant ou changeant de responsabilités).
