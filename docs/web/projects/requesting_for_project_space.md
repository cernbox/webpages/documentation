# Requesting for project space

Project Space for Experiments/Projects:

If your experiment or project requires, and **DOES NOT ALREADY HAVE SPACE on EOS**, you can request for space: fill in the [Request project space on EOS/CERNBOX](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request-storage-space&se=CERNBox-Service) form in Service-Now.

You will be asked to provide the following information initially:

1. What is the experiment/project name? It will be used for the directory name.

2. A short description of the experiment/project.

3. Space needed (by default 1TB).

4. Pls provide a [Service Account](https://account.cern.ch/account/Management/NewAccount.aspx). All the data will remain attached to this account; the account can be transferred to another person, if necessary (e.g. someone leaving or changing responsibilities).

