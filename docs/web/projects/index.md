# Projects and other data sources

## Projects

Projects are used for storing data for an activity across different users. To request for project space, see section [Request for project space](requesting_for_project_space.md). Please note that large experiments/activities have dedicated EOS areas, and users can consult their experiment storage managers about this (see 'EOS for Physics' [Service Level Description](https://cern.service-now.com/service-portal?id=kb_article&n=KB0002879) for more details).

To have an overview of the projects to which the logged in account has access to, it's possible to list them by clicking in `Projects` on the left menu.

Now it's also possible to see and recover files of projects by right clicking on a project and then on "Open trashbin".

![Project trashbin](../../assets/screenshots/projects-trashbin.png)

Project administrators can now see the shares associated with a file/folder and will be able to edit them. Any shared created by an admin, will be associated with the service account that owns the project.

## Other data sources

Other data sources will be added to CERNBox as well. Access to the other EOS instances or the HPC home directories, for example.
