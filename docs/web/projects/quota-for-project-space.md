# Check the quota

To check the quota for your project you need to impersonate the **Service Account** holder of the project.
 

1. Log into lxplus with your Primary Account
2. Impersonate the relevant Service Account by obtaining a kerberos token:
```bash
kinit <your-service-account>
```
3. Run the eos command:
```bash
eos root://eosproject.cern.ch quota /eos/project/<initial>/<project-name>
```
![](../../assets/images/project-quota-1.png) 

In the example, project "swanee" is using the quota as follows: 9.81 MB of the allowed 1.00 TB; 78 files of the allowed 1.00M files:

```bash
[swanee@lxplus701 ~]$ eos root://eosproject.cern.ch quota /eos/project/s/swanee
# pre-configuring default route to /eos/user/s/swanee/
# -use $EOSHOME variable to override

By user:
┏━> Quota Node: /eos/project/
┌──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┐
│user      │used bytes│logi bytes│used files│aval bytes│aval logib│aval files│ filled[%]│vol-status│ino-status│
└──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┘
 swanee       19.62 MB    9.81 MB         78    2.00 TB    1.00 TB     1.00 M     0.00 %         ok         ok
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

By group:
┏━> Quota Node: /eos/project/
┌──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┬──────────┐
│group     │used bytes│logi bytes│used files│aval bytes│aval logib│aval files│ filled[%]│vol-status│ino-status│
└──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┴──────────┘
 def-cg      218.07 TB  109.04 TB    28.43 M        0 B        0 B          0   100.00 %    ignored    ignored
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛

```

Note that until you obtain again a kerberos token for your Primary Account, any further command you issue in that lxplus session will be executed with the Service Account's credentials.

It is possible to define the `EOS_MGM_URL=root://eosproject.cern.ch` variable beforehand. In which case, you would not need `root://eosproject.cern.ch` in the EOS command.
