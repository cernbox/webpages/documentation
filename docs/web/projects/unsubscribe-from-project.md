# Subscribe / Unsubscribe from a project

If you [login to your CERNBox](https://cernbox.cern.ch) space via a Web browser, in 'Your projects', you can see in which projects you are considered to be a member. That is, you are in one of the e-groups that have been granted access to the project:

   - `cernbox-project-<projectname>-admins`  (members can add new administrators. Administrators can add users to `-writers` and `-readers` egroups) 
   - `cernbox-project-<projectname>-writers` (members can read, write, delete in the project space. The `-admins` e-group is already included in the `-writers` egroup)
   - `cernbox-project-<projectname>-readers` (members can only read the files in the project space)

If you are expected to be part of a project and it does not show up in 'Your projects', you should contact the project administrators requesting to be added as reader or writer, as appropriate. To contact the project administrators, send an email to:

`cernbox-project-<projectname>-admins@cern.ch` 

Conversely, if you deem that you have been wrongly added into a project, you should contact them requesting that they remove you as a member.

In both cases, please note that the change in 'Your projects' view is not instantaneous and may take up to a few days, due to a number of synchronization processes upstream of CERNBox.
