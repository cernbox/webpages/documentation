# Manage access

Access in project spaces is controlled:

* to the ENTIRE project via the `cernbox-project-<projectname>-writers` and `-readers` e-groups (you can check their membership at [https://egroups.cern.ch](https://egroups.cern.ch)): see [manage project space](manage_project_space.md).
* to individual folders (and their sub-folders) via the Authenticated Share method in CERNBox (in a Web browser).
   
> All the folders of the project can be shared by Authenticated Share and Link Share by members in the `-admins` e-group ONLY. This group includes by default the Service Account that was designated when creating the project space, which owns all the project's files.
> The admin user logs into his/her CERNBox, goes to the project in 'Your projects', and can then share a file/folder. 

## Check (via LXPLUS) which e-groups and users have access to the project space 
 
 Admins for the project can check who can access the project space by running the following commands on LXPLUS:
 
  - Login with the project Service Account, or your own account (if you are an admin for the project space).
 
  - Run the command:

```
eos root://eosproject.cern.ch attr ls /eos/project/<initial>/<project-name>/<path-to-folder>
```

Example:

![](../../assets/images/project-attr-2.png)


```
[swanee@lxplus701 ~]$ eos root://eosproject.cern.ch attr ls /eos/project/s/swanee/plots
sys.acl="egroup:cernbox-project-swanee-readers:rx,egroup:cernbox-project-swanee-writers:rwx+d,u:99090:rwx+d"
sys.allow.oc.sync="1"
sys.eos.btime="1584701732.657186956"
...
```

It is possible to define the EOS_MGM_URL=root://eosproject.cern.ch variable beforehand. In which case, you would not need root://eosproject.cern.ch in the EOS command.

In the example above:

* `egroup:cernbox-project-swanee-writers:rwx+d` means that users in this e-group can read, write, execute and delete files/folders in this folder 
* `egroup:cernbox-project-swanee-readers:rx` means that users in this e-group can read files/folders in the folder
* `u:99090:rwx+d` means that the user with userid `99090` can read, write, execute, delete files/folders in the folder. The user was shared this folder using the Web interface. 
 
To find out which user has userid `99090`, use the command `getent passwd <userid>`

 ```
 $ getent passwd 99090
 ebocchi:*:99090:2763:Enrico Bocchi,31 2-010,+41227674203,:/afs/cern.ch/user/e/ebocchi:/bin/bash
```
