# Data security

The same data safety and recovery options of the previous system are still available.

## Versions

To access the versions of a file, in the right-click menu click in `Details` then on `Versions`. The system keeps the 10 most recent versions of each file, as well as a version from each of the last few days.

## Trash bin

All files deleted from your personal space will land on the Trashbin and stay there for 6 months. Admins of a project can also see the trashbin of their project spaces, as described in [Projects](../projects/index.md)


