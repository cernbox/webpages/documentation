# Retrouver/restaurer des fichiers/dossiers
Quand vous supprimez un fichier ou un dossier de votre CERNBox, il n'est pas effacé complétement, mais reste dans un espace "poubelle" pour une durée d'environ 6 mois.

Par conséquent, vous pouvez retrouver ce que vous avez supprimé à travers la page ***Fichiers supprimés*** qui se trouve dans la barre latérale gauche de l'écran. 

Cliquez alors sur le bouton  ![](../../assets/images/3_dot.png)  sur la ligne du fichier que vous voulez restaurer et cliquez sur le bouton **&#8634;** depuis le menu déroulant qui s'est affiché. Veuillez voir [ici](../../advanced/restore_from_trash.md) (en Anglais) pour plus de détails.

## Restaurer une version antérieure

Pour restaurer un fichier dans une version antérieure, 

- accèdez au fichier depuis votre page d'accueil CERNBox, 
![Restaurer une version antérieure - 1](../../assets/images/restore_versions_1.png) 
- cliquez sur l'onglet ***Versions*** dans la fenêtre qui s'ouvre à droite sur l'écran.
- Choississez la version que vous voulez restaurer et cliquez sur le bouton **&#8634;** correspondant.
![Restaurer une version antérieure - 2](../../assets/images/restore_versions_1_and_2.png)

 

