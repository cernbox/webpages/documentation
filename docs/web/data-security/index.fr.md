# Sécurité des données

Les mêmes options de sécurité et de récupération des données du système précédent sont toujours disponibles.

## Version

Pour accéder aux versions d'un fichier, dans le menu clic-droit cliquez sur `Détails` puis sur `Versions`.

## Poubelle

Tous les fichiers supprimés atterriront dans la corbeille et y resteront pendant 6 mois. Désormais, les administrateurs d'un projet peuvent également voir sa corbeille, comme décrit dans [Projets](../projects/index.md#project)

