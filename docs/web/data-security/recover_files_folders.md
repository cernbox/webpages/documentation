# Recover files/folders
When you delete a file or a folder in CERNBox, it is not permanently deleted at that very moment. Instead, it is moved into the trash bin where it is kept for a maximum of six months.

You can find your deleted files by clicking on ***Deleted files*** in the lower-left corner of your CERNBox Files page.

Click on the ![](../../assets/images/3_dot.png)  button next to the file you want to restore and click on the **&#8634;** button from the small menu which appears.  See [here](../../advanced/restore_from_trash.md) for further details.

## Recover a previous version

You can also recover a **previous version** of a file:

1.  Click on the file in your Files page

 ![Recover previous version Step 1](../../assets/images/restore_versions_1.png)
-  In the window that gets displayed on the right-side of the page, click on ***Versions***.

-  Then click on the **&#8634;** button beside the file you want to restore.

 ![Recover previous version Step 2](../../assets/images/restore_versions_1_and_2.png)
 

