**Matrix of features for file versioning in different OS/Devices**

The matrix below shows features for versioning of files for different OS/Devices and different access methods.

![versions matrix](../../assets/images/versions-matrix.png)

Backups are available, but, currently, users should contact [CERNBox Service](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=CERNBox-Service) to request for a restore.

