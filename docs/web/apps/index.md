# Overview

## Online viewing/editing capabilities

The new CERNBox has the following application/file extensions integrations:

| Application | Opens | Creates | Allows editing |
| ----------- | ----- | ------- | :------------: |
| Microsoft Office 365 | Office files | .docx, .pptx, .xlsx | :material-check: |
| CodiMD | Markdown files (.md, .zmd) | .md, .zmd | :material-check: |
| Draw.io | diagrams (.drawio, .vsdx) | .drawio | :material-check: |
| Text editor | text files (.txt) and .js, .json, .xml, .py, .php, .yaml | .txt | :material-check: |
| PDF viewer | .pdf |  | :material-close: |
| Jupyter viewer | .ipynb |  | :material-close: |
| Open in SWAN | .ipynb |  | :material-check: |
| IFC viewer | .ifc |  | :material-close: |
| ROOT viewer | .root |  | :material-close: |
| Media viewer | photos and videos (.png, .jpg, .jpeg, .gif, .mp4, .webm, .ogg) |  | :material-close: |

## Collaboration hub

The first 2 apps (MS Office 365 and CodiMD), also have concurrent editing capabilities. For the others with editing capabilities, mechanisms were put in place to prevent user' conflicting changes being done at the same time. This makes the new CERNBox a true collaboration hub.

## Opening with non default apps

Clicking on a file will open it with the default application. To open with a different one, right click the file to see the options.

![Open a file with different apps](../../assets/screenshots/apps-context.png)

## Application URLs

Just like all other URLs (see [Universal URLs](../access.md#universal-urls)), the application URLs are also shareable with someone who has access to the file. They can be bookmarked so that you always land on the same application and on the same file.

![Example document opened in MS Office 365](../../assets/screenshots/word.png)


## Learn more about file apps
- [CodiMD](codimd.md)
- [MS Office 365](office_online.md)
- [Notebook viewer and editor](notebook_viewer_and_editor.md)
- [ROOT](root_analysis.md)
