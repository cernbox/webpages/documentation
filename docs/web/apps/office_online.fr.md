# Lecteur et éditeur Microsoft Office Online

Microsoft fournit des éditeurs et lecteurs en ligne pour les documents Word, Excel et PowerPoint. Ils ont été intégrés à CERNBox afin de permettre l'édition en collaboration de document Office.

La lecture et l'édition de documents Microsoft est supporté sur n'importe quel répertoire ainsi que sur n'importe quel liens partagé ou public. Si différents utilisateurs édite le même document, ils seront tous notifié sur qui d'autre édite le document. Quand un dossier est partagé avec un lien public et le mode écriture est activé, toute personne en possession du lien peut éditer tout document Microsoft Office dans ce dossier. Dans ce cas, les modifications sont faites au nom du propriétaire du dossier, et 'Guest xyz' est affiché en tant que nom d'utilisateur.

![Session d'edition en colaboration dans Microsoft Office Online](../../assets/images/office_online.png)

## Gestion des conflits 

Si un document office est edité en parallèle avec Office Online et avec un autre moyen d'accès (hors-ligne depuis un client de synchronisation, ou en ligne depuis un [lecteur réseau](../../desktop/other-access-methods/samba.md) ou FUSE), il est possible que Office Online génere une copie du document si il détecte qu'une modification a eu lieu en dehors d'une session Office Online. Dans ce cas, un message d'erreur 'Session expired' apparait et les sauvegardes sont desactivées. Vous êtes ensuite demandé de fermer Office Online et de résoudre les conflits en inspectant les copies du fichier existantes.
