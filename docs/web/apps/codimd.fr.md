# CodiMD pour les fichiers Markdown

[CodiMD](https://github.com/hackmdio/codimd) est un éditeur Markdown populaire, doté d'une double fenêtre avec une vue de texte brut et une vue rendue, et permettant une collaboration en temps réel entre les utilisateurs. Il prend en charge un grand nombre de [fonctionnalités](https://cernbox-codimd.web.cern.ch/features), y compris l'intégration d'images, une vue en mode diapositive, des équations LaTeX, des blocs de code, des diagrammes, etc.

![Montage dans CodiMD](../../assets/images/codimd.png)


* ![Create and edit .md files](../../assets/images/md.png) CERNBox intègre de manière transparente CodiMD pour stocker et afficher les fichiers Markdown (`.md`), permettant une édition collaborative avec les utilisateurs avec lesquels vous partagez le document. Veuillez noter que les collaborateurs en mode lecture seule pour le moment ne sont pas en mesure de suivre en temps réel le contenu mis à jour par ceux qui ont des droits d'édition, et doivent actualiser la page pour accéder au contenu le plus récent.


    Lors de l'édition de fichiers `.md`, les utilisateurs ont la possibilité d'intégrer des images qui résident déjà dans CERNBox, en cliquant sur l'icône CERNBox dans la barre d'outils CodiMD : dans ce cas, un _lien public_ est créé à la volée et inséré dans le document pour que CodiMD rende l'image.



* ![Create and edit .zmd files with option to embed pictures from local storage](../../assets/images/zmd.png) De plus, CERNBox permet la création de _fichiers Markdown avec images_ (`.zmd`), où les utilisateurs ont également la possibilité d'intégrer des images à partir du stockage local. Dans ce cas, les images seront stockées dans le fichier sous la forme d'un ensemble compressé.
