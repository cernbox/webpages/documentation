# Lecteur de fichier  ROOT

Les fichiers de type ROOT sont reconnaissables à leur pictogramme ![](../../assets/images/cernbox_icons_root.png).

La visualisation d'un fichier ROOT s'effectue dès que vous cliquez sur le nom d'un fichier. Une nouvelle fenêtre s'ouvre :
- à gauche, les informations sur le fichier et sur la façon de le présenter 
- à droite, les résultats, selon le format choisi

![Visualisation fichier ROOT](../../assets/images/root_1_f.png)

## Les différents formats de présentation
Par exemple, vous pouvez choisir de voir le graphique les uns après les autres, ou plus simplement les 3 graphiques sur le même écran :


- Dans le menu déroulant, choisir l'option (par défaut '*simple*')

![](../../assets/images/root_layout.png)

- puis cliquer sur le bouton ***Reload with selected layout***

![Layout simple](../../assets/images/root_2.png)

![Layout 2 fenêtres dans un écran](../../assets/images/root_3.png)

| Layout | Description  |
| ----------- | ------------ |
| simple | valeur par défaut; le résultat s'affiche en "plein écran"
| collapsible | ouvre le résultat dans un onglet prenant la plus grande largeur possible et réductible en hauteur. Si plusieurs résultats, ceux ci sont affichés les uns après les autres
| flex | Ouvre le(s) résultat(s) dans des fenêtres déplacables et dont on peu faire varier la table
| tabs | ouvre les résultats dans des onglets différents <!-- TO BE CHECKED <mark> was not able to do it in Windows+ie on hsimple.root</mark> -->
| grid axb | a:1..4, b:2..4<br>divise l'écran d'affichage, horizontalement en <i>a</i>, verticalement en <i>b</i><br>les possibilités offertes sont : 1x2, 2x2, 1x3, 2x3, 3x3, 4x4 |

## Fonction zoom

Vous pouvez également agrandir une partie d'un graphique, simplement en la sélectionnant à l'écran. 

dans l'exemple suivant, zoom sur les queues de l'histogramme

![Exemple de zoom sur un graphiue](../../assets/images/root_5.png)
