# Applications

## Capacités de visualisation/édition en ligne

La nouvelle CERNBox a les intégrations d'extensions d'applications/fichiers suivantes :

| Application | Opens | Creates | Allows editing |
| ----------- | ----- | ------- | :------------: |
| Microsoft Office 365 | Office files | .docx, .pptx, .xlsx, .one | :material-check: |
| CodiMD | markdown files (.md, .zmd) | .md | :material-check: |
| Draw.io | diagrams (.drawio, .vsdx) | .drawio | :material-check: |
| Text editor | text files (.txt) and .js, .json, .xml, .py, .php, .yaml | .txt | :material-check: |
| PDF viewer | .pdf |  | :material-close: |
| Jupyter viewer | .ipynb |  | :material-close: |
| Open in SWAN | .ipynb |  | :material-check: |
| IFC viewer | .ifc |  | :material-close: |
| ROOT viewer | .root |  | :material-close: |
| Media viewer | photos and videos (.png, .jpg, .jpeg, .gif, .mp4, .webm, .ogg) |  | :material-close: |

## Centre de collaboration

Les 2 premières applications (MS Office 365 et CodiMD) ont également des capacités d'édition simultanées. Pour les autres avec des capacités d'édition, des mécanismes ont été mis en place pour empêcher que des modifications conflictuelles de l'utilisateur ne soient effectuées en même temps. Cela fait de la nouvelle CERNBox un véritable hub de collaboration.

## Ouverture avec des applications non par défaut

Cliquer sur un fichier l'ouvrira avec l'application par défaut. Pour ouvrir avec un autre, faites un clic droit sur le fichier pour voir les options.

![Ouvrir un fichier avec différentes applications](../../assets/screenshots/apps-context.png)

## URL des applications

Comme toutes les autres URL (voir [URL universelles](../access.md#universal-urls)), les URL d'application peuvent également être partagées avec une personne ayant accès au fichier. Ils peuvent également être mis en signet afin que vous atterrissiez toujours sur la même application et sur le même fichier.

![Exemple de document ouvert dans MS Office 365](../../assets/screenshots/word.png)

Pour partager l'URL d'un fichier sans choisir l'application à utiliser (en laissant cela au destinataire), il est possible de copier un "lien direct" à partir de la barre latérale droite.