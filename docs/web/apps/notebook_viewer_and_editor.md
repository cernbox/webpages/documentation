#  Notebook viewer and editor
CERNBox is one of the key elements for the [SWAN](http://swan.web.cern.ch/) (Service for Web based ANalysis) project.

* Analyse data without the need to install any software
* Jupyter notebook interface as well as shell access from the browser
* Use CERNBox as your home directory and synchronise your local user storage with the cloud
* Access experiments' and user data in the CERN cloud
* Share your work with your colleagues thanks to CERNBox
* Document and preserve science - create catalogues of analyses: encourage reproducible studies and learning by example

Notebook files are marked with the ![](../../assets/images/cernbox_icons_notebook.png) icon.

![Notebook made by a student](../../assets/images/notebook_1.png)
