# Editeur et lecteur de notebook
CERNBox est l'un des éléments clé du projet [SWAN](http://swan.web.cern.ch)(Service for Web based ANalysis)(en anglais).

Intérêt : 
* Analyse de données sans avoir besoin d'installer de logiciel.
* Interface du bloc-note jupyter ainsi qu'un accès au shell depuis le navigateur
* Utilise CERNBox en tant que répertoire personnel et synchronise votre donnée locale avec le cloud.
* Acceder aux données des expériences et à vos données personnelles sur le cloud du CERN
* Partager votre travail avec vos collègues grâce à CERNBox
* Documenter et préserver - créer un catalogue d'analyses : encourager des études reproductible et l'apprentissage par l'exemple

Les fichiers bloc-note sont marqués avec l'icone ![](../../assets/images/cernbox_icons_notebook.png).

![Notebook fait par un étudiant](../../assets/images/notebook_1.png)
