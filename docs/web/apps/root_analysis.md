# ROOT files reader

Root type files are recognizable by their pictogram ![](../../assets/images/cernbox_icons_root.png).

The visualization of a root file is carried out as soon as you click on the name of a file.
A new window opens:
* On the left, information on the file and on how to present it
* on the right, the results, depending on the chosen format

![Visualization root file](../../assets/images/root_1.png)

## The different presentation formats


For example, you can choose to see the graphic one after the other, or more simply the 3 graphics on the same screen:
* In the drop -down menu, choose the option (default '_simple_')

![](../../assets/images/root_layout.png)

* Then click on the button ***Reload with selected layout***

![Simple layout](../../assets/images/root_2.png)

![Layout 2 windows in a screen](../../assets/images/root_3.png)

| Layout | Description |
| ---------- | ----------- |
| Simple | Default value; <br> The result is displayed in "full screen" |
| Collappsible | Open the result in a tab taking the greatest possible width and reducible in height. If several results, these are displayed one after the other. |
| Flex | Opens the result(s) in moving windows |
| Tabs | Opens the results in different tabs |
| Grid AXB | A: 1..4, b: 2..4 <br> divides the display screen, horizontally in <i> a </i>, vertically in <i> b </i> <br> The possibilities available are: 1x2, 2x2, 1x3, 2x3, 3x3, 4x4 |

## Zoom
You can also enlarge part of a graphic, simply by selecting it on the screen.
In the following example, zoom in on the tails of the histogram

![Example of zoom on a graph](../../assets/images/root_5.png)