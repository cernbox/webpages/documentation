# CodiMD for Markdown files

[CodiMD](https://github.com/hackmdio/codimd) is a popular Markdown editor, featuring a double window with a raw text view and a rendered view, and allowing real-time collaboration among users. It supports a large number of [features](https://cernbox-codimd.web.cern.ch/features), including embedding images, a slide mode view, LaTeX equations, code blocks, diagrams, etc.

![Editing in CodiMD](../../assets/images/codimd.png)


* ![Create and edit .md files](../../assets/images/md.png) CERNBox seamlessly integrates CodiMD to store and display Markdown (`.md`) files, allowing collaborative editing with the users you share the document with. Please note that collaborators in read-only mode at the moment are not able to follow in real-time the content as updated by those with editing rights, and need to refresh the page to access the newest content.


    When editing `.md` files, users have the possibility to embed pictures that already reside in CERNBox, by clicking on the CERNBox icon in the CodiMD toolbar: in such a case, a _public link_ is created on the fly and inserted in the document for CodiMD to render the image.



* ![Create and edit .zmd files with option to embed pictures from local storage](../../assets/images/zmd.png) In addition, CERNBox allows the creation of _Markdown files with images_ (`.zmd`), where users also have the option to embed pictures from local storage. In this case, the pictures will be stored within the file as a zipped bundle.
