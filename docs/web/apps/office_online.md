# Microsoft Office Online viewers and editors

Microsoft provides online viewers and editors for Word, Excel, and PowerPoint documents. These have been integrated in CERNBox to allow collaborative editing of Office documents.

Viewing and editing of Microsoft documents is supported on any folder as well as any share or public link. If multiple users edit the same file, every user is notified about who else is sharing the editing session. When a folder has been shared with a public link and write mode is enabled, anyone with the public share is allowed to interactively edit Microsoft Office documents into the folder. In such a case, modifications take place on behalf of the folder's owner, and 'Guest xyz' is shown as the user name in the Office Online editing sessions.

![Collaborative editing session in Microsoft Office Online](../../assets/images/office_online.png)

## Conflicts handling

If an Office document is concurrently edited with Office Online and with another access path (offline from a sync client, or online from a [network drive](../../desktop/other-access-methods/samba.md) or FUSE mount), Office Online may generate a conflict copy of the document being edited when it detects that a modification took place outside the Office Online editing session. In such a case, you will be shown a 'Session expired' error message and further saving is disabled. You are then requested to close Office Online and resolve the conflict by inspecting the available copies of the file.
