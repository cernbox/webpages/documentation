# Available storage
By default, CERNBox allocates a quota of **1 TB** and a limit of **1 M files**, with a maximum size of a single file of 50GB, for main accounts and a quota of **20 GB** for secondary and service accounts. Project spaces may have larger quotas. For most use cases, this is more than enough storage space. However, it may be  interesting to know where to find information about your space usage.

## Used space

Click on your user round badge at the upper right corner of your home page: the quota is displayed in the menu.

Another way for you to check your personal space or your experiment space, you can do the following :

- Log in into lxplus using your username : `ssh username@lxplus.cern.ch`
- Execute the following command : `export EOS_MGM_URL=root://eosuser.cern.ch`
- Then issue the following to display the quota, both as used space and count of files : `eos quota`

Your quota is calculated as follows:

* The used space is the sum of the size of all the files stored in your space.
* When other users share files with you, the shared files count against the original share owner’s quota. 
* When you share a folder and allow other users or groups to upload files to it, all uploaded and edited files count against your quota.
* Deleted files that are still in the trash bin do not count against quotas.
* The older file versions (up to 20) are counted against quotas.
* If you create a public share via URL, and allow uploads, any uploaded files count against your quota.

## Other limits
As mentioned, files can be up to 50GB in size, and any access method (sync, FUSE, xrootd, network drive) would raise an error if you attempt to overcome this limit.

Furthermore, for web transfers the limit is reduced to 8GB or 30 minutes of transfer time, whichever comes first. If you need to transfer larger files, you can use [EOS web sites](../advanced/web-pages/expose_files_in_website.md). Yet, we recommend to chunk very large files to help users downloading them in the absence of transfer recovery mechanisms.

<!-- TO BE CHECKED  -->
<!-- how to find the space used for a project? -->
