# Welcome to the new CERNBox!

This documents aims to explain the basic functionalities of the new and redesigned [CERNBox](https://cernbox.cern.ch){target=_blank}, as well as introduce the brand new features exclusive to it.

## What's changed ?

Everything available in the previous system is also available with a new modern look in the new, including the 1TB of storage space and the security and confidentiality assurances provided by the CERN Data Center.

The new interface aims to improve the usability. Check the changes in the section [Interface changes](interface-changes.md).

## New features

We invite you to read the topics presented on the left menu.
These include explanations to the new major functionalities introduced:

* [More modern and intuitive UI](interface-changes.md)
* [Universal URLs](access.md#universal-urls)
* [Single file sharing](sharing/index.md)
* [Improved app ecosystem](apps/index.md)
* [Improved Projects integration](projects/index.md)
* [Lightweight accounts support](access.md#accounts-supported)
* [Access denials](projects/manage_project_space.md#sharing-the-resources-inside-the-project)
* [Notifications](notifications/index.md) (beta)

## Did you know you can work with Markdown and Office files?

* [How to use Markdown](apps/codimd.md)
* [How to use Office](apps/office_online.md)

!!! info "Other features will be coming in the coming months"

    * Integrations with other services (Indico already available)
    * Improved search
    * Auditing and reporting
    * Backup restore UI
    * Improved Sync Client integration



## Desktop Requirements

When using a desktop device like a PC, Notebook including Microsoft Surface, the following browsers are supported:

| Browser | Supported version | Operating System              |
|---------|-------------------|-------------------------------|
| Chrome  | Latest 2 versions | Windows 10+ macOS 12.5+ Linux |
| Firefox | Latest 2 versions | Windows 10+ macOS 12.5+ Linux |
| Edge    | Latest 2 versions | Windows 10+                   |
| Safari  | 16+               | macOS 12.6+                   |
You can find the supported versions [here](https://browserslist.dev/?q=bGFzdCAyIHZlcnNpb25z){target=_blank}
