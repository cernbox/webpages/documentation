# Espace de stockage disponible

Chaque utilisateur CERNBox dispose d'un espace de stockage personnel de **1 To**, avec une limite de **1 M de fichiers**, avec une taille de fichier maximale de 50 Go, pour les comptes principaux et un quota de **20 Go** pour les comptes secondaires et de service. Les espaces projet ont normalement des limites superieures. Par conséquent, l'espace utilisé dans votre CERNBox ne devrait pas devenir un problème. Néanmoins, il est intéressant de connaître la méthode de calcul de cet espace.


## Combien d'espace déjà utilisé

Pour voir combien d'espace vous utilisez sur l'ensemble du quota alloué, cliquer sur votre badge dans le coin supérieur droit de votre page d'accueil CERNBox.

Une autre façon de vérifier votre espace personnel ou commun est la suivante :

- Connectez vous a lxplus avec votre utilisateur : `ssh username@lxplus.cern.ch`
- Entrez la commande suivante : `export EOS_MGM_URL=root://eosuser.cern.ch`
- Finalement, entrez la commande suivante : `eos quota`     

Le calcul de l'espace utilisé s'effectue de la manière suivante :

* L'espace utilisé correspond à la somme de la taille des fichiers stockés dans votre espace CERNBox.
* Quand une autre personne a partagé un fichier ou un dossier avec vous, l'espace occupé n'est pas décompté de votre quota, mais bien de celui du propriétaire du fichier. 
* En revanche, si vous avez partagé un de vos dossiers avec droit d'écriture, tout fichier ou dossier qui y sera ajouté sera décompté sur votre quota. Idem si vous avez partagé un dossier à travers un lien hypertexte.
* Les fichiers supprimés, même lorsqu'ils sont encore dans l'espace "poubelle", ne sont plus décompté sur votre quota.
* Mais les versions antérieurs de vos fichiers (jusqu'au nombre de 20) le sont.


## Autres limites
Comme mentionné, les fichiers ont une taille maximale de 50GB, donc touts methodes d'accès (sync, FUSE, xrootd, network drive) produisent un erreur si on essaye de transferer un fichier plus grand.

En plus, pour les transferts via web la limite est de 8GB ou 30 minutes de transfert, n'importe quel seuil est depassé en première. Si vous avez besoin de transferer des grands fichiers, vous pouvez utiliser des [sites web sur EOS](../advanced/web-pages/expose_files_in_website.md). Neanmoins, on vous suggère de couper les grands fichiers en parties pour aider les utilisateurs à les télécharger.

