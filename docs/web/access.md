# Access instructions

## How to get an account?

The initial and **mandatory step** to using your CERNBox is to create it. Use any Web browser to go to [https://cernbox.cern.ch](https://cernbox.cern.ch) and login via the CERN Single Sign-On page with your username and password. 

## How to access?

The service is available at the address [cernbox.cern.ch](https://cernbox.cern.ch){target=_blank}.

![The new CERNBox UI](../assets/screenshots/main-window.png)

Other possible access methods include:

* Windows: the `H:` drive for home spaces and the `\\cernbox-drive` network share for all other spaces
* Sync and mobile clients
* EOS FUSE mounts (i.e lxplus, SWAN, ...)

## Accounts supported

The new CERNBox uses the new CERN SSO and it now **allows signing in with other accounts, like social or external**.

Unlike the normal CERN accounts, these do not have storage associated with them. They can, however, collaborate on [shared resources](sharing/index.md) or on [Projects](projects/index.md)

![The UI for social/lightweight accounts](../assets/screenshots/lightweight.png)

## Universal URLs

For users who have used lxplus or had to synchronize a share in the past, they will recognize something new in the URL:

{==

https://new.cernbox.cern.ch/files/spaces**/eos/user/<letter\>/<username\>**

==}

CERNBox, which uses the [EOS storage system](https://eos.web.cern.ch){target=_blank} under the hood, now exposes its full namespace. This means most of our access methods (excluding the mobile client, for now) have a consistent and more intuitive view.

!!! tip

    It also means that now it's possible to just copy your URL and share it directly with someone else. If that person has permissions, he/she will be able to open it.

    This also works with direct links to [applications](apps/index.md).
