# Instructions d'accès

## Comment accéder ?

Le service est disponible sous l'adresse [cernbox.cern.ch](https://cernbox.cern.ch){target=_blank}.

![La nouvelle interface utilisateur CERNBox](../assets/screenshots/main-window.png)

Vous pouvez vous connecter avec vos identifiants CERN. Les titulaires de comptes de service peuvent également se connecter avec eux.

Les autres méthodes d'accès sont :

* Windows : le lecteur `H:` pour les espaces personnels et le partage réseau `\\cernbox-drive` pour tous les autres espaces
* Les clients de synchronisation et mobiles
* Les systèmes qui montent EOS FUSE (i.e. lxplus, SWAN, ...)

## Comptes pris en charge

La nouvelle CERNBox utilise le nouveau CERN SSO et elle **permet désormais de se connecter avec d'autres comptes, comme social ou externe**.

Contrairement aux comptes CERN normaux, ceux-ci n'ont pas de stockage associé. Ils peuvent cependant collaborer sur des [ressources partagées](sharing/index.md) ou sur des [Projets](projects/index.md)

![L'interface utilisateur pour les comptes sociaux/lightweight](../assets/screenshots/lightweight.png)

## URL universelles

Pour les utilisateurs qui ont utilisé lxplus ou qui ont dû synchroniser un partage dans le passé, ils reconnaîtront quelque chose de nouveau dans l'URL :

{==

https://new.cernbox.cern.ch/files/spaces**/eos/user/<letter\>/<username\>**

==}

CERNBox, qui utilise le [système de stockage EOS](https://eos.web.cern.ch){target=_blank} sous le capot, expose désormais son espace de noms complet. Cela signifie que la plupart de nos méthodes d'accès (à l'exception du client mobile, pour l'instant) ont une vue cohérente et plus intuitive.

!!! tip

    Cela signifie également qu'il est désormais possible de simplement copier votre URL et de la partager directement avec quelqu'un d'autre. Si cette personne a des autorisations, elle pourra l'ouvrir.

    Cela fonctionne également avec des liens directs vers [applications] (apps/index.md).
