# Sharing and permissions

As with the previous system, it's possible to share folders with other authenticated users. But now, **it's also possible to share single files** in both read and write modes!

To share a folder/directory, just right click it and click in `Share`. This will open the right sidebar with options to create authenticated or public share.

Learn, how to create

* [authenticated shares with people](auth-share.md)
* [public shares](public-share.md)
* [Denial permissions inside projects](../projects/manage_project_space.md#sharing-the-resources-inside-the-project)


<iframe width="576" height="360" frameborder="0" src="https://weblecture-player.web.cern.ch/?year=2023&amp;id=1275596" ?showtitle="true" allowfullscreen></iframe>
[Tutorial about CERNBox Share And Auth Share on twiki.cern.ch](https://twiki.cern.ch/twiki/bin/view/Edutech/CERNBoxShareAndAuthShare){:target="_blank"}



!!! info Note
    Currently, users are not allowed to directly change the ACL's in the unix command-line when in EOSUSER (the storage backend of CERNBox). A CLI interface will be provided in the future.
## Permissions inheritance

Shares/public links added to any parent folders will be shown in the `People` and `Links` views. This allows a better understanding on why someone has already access to a file/folder.

In the `People` view, the users given permission on the parent folder will be visible without the option to change their role (viewer/editor) nor the option to remove the share.

In the `Links` view, all the links will be visible, with the mention that it comes `Via <folder name>`.

![File already accessible via a parent link](../../assets/screenshots/shared-via.png)
