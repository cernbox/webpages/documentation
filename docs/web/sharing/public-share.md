# Public shares

## Create public share 
To share a folder/file without requiring users to be logged in, you can create a `Public link`. The change compared to the previous system is that now it's also possible to share a single file with the `Editor` permission publicly.

You can create multiple public links. Public link with the name "Quick link" is the one that will be copied when you click on quick actions "Copy quick link".

You can additionally set several options for the public links:

* Role: 
    * Viewer (can view and download content)
    * Editor (can view, download, edit, delete content)
    * Uploader (Anonymous upload: can upload but existing contents are not revealed)

* Expiration time (in :material-dots-vertical:)

* Password (in :material-dots-vertical:)

You can delete the link in :material-dots-vertical:

![Public share menu](../../assets/screenshots/public-link.png)


## Overview of public links

To have an overview of all public links created by you, just click in `Shares` - `Shared publicly` on the left menu.

## Considerations on usage

Public links are suitable to anonymously serve files in case of relatively small amount of data.

If you need to transfer larger files, we recommend you to use [EOS web sites](../../advanced/web-pages/expose_files_in_website.md).