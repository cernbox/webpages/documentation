# Shared with me

If the sharer selects option to notify you about the share, you will receive the email with the link to that share.

The `Shared with me` page will show everything that was shared with the logged in account. In the table overview you can see who shared with you ("Shared by"). You can also see if the share was shared with you or an e-group ("Shared with").


You can hide shares, they will disappear from the shares list. Hidden shares are not removed, you can always click in `Show hidden shares` to see them and unhide them later.

![Shared with me](../../assets/screenshots/shares-overview.png)

To improve the usability, it's now possible to select multiple shares and batch hide/unhide them. It's also possible to group the shares by different criteria (owner, time, etc).

To access the content inside of a share, just click on any share, whereas to access the details such as the path of a shared resource, right-click on a share and select Details.