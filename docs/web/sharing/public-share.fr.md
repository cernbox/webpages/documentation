# Partages publics

## Créer un partage public
Pour partager un dossier/fichier sans obliger les utilisateurs à se connecter, vous pouvez créer un "lien public". A noter qu'il est désormais possible de partager publiquement un seul fichier avec l'autorisation "Éditeur".

Vous pouvez créer plusieurs liens publics. Le lien public nommé "Lien rapide" est celui qui sera copié lorsque vous cliquerez sur les actions rapides "Copier le lien rapide".

Vous pouvez en outre définir plusieurs options pour les liens publics :

* Rôle : 
    * Spectateur (peut afficher et télécharger du contenu)
    * Editeur (peut afficher, télécharger, modifier, supprimer du contenu)
    * Téléchargeur (Téléchargement anonyme: peut télécharger mais le contenu existant n'est pas révélé)

* Délai d'expiration (en :material-dots-vertical:)

* Mot de passe (en :material-dots-vertical:)

Vous pouvez supprimer le lien dans :material-dots-vertical :

![Menu de partage public](../../assets/screenshots/public-link.png)


## Aperçu des liens publics

Pour avoir un aperçu de tous les liens publiques que vous avez créé, il vous souffit d'ouvrir la page `Partages` - `Partagés publiquement` dans le menu à gauche.

## Considerations d'utilisation

Les liens publics sont adaptés pour servir des fichiers de manière anonyme dans le cas d'une quantité relativement faible de données.

Si vous devez transférer des fichiers plus volumineux, nous vous recommandons d'utiliser les [sites web EOS](../../advanced/web-pages/expose_files_in_website.md).