# Partages authentifiés

Comme avec le système précédent, il est possible de partager des dossiers avec d'autres utilisateurs authentifiés. Mais maintenant, **il est également possible de partager des fichiers uniques** en mode lecture et écriture !

Pour partager un dossier/répertoire, faites un clic droit dessus et cliquez sur "Partager". Cela ouvrira la barre latérale droite, sur l'option "Personnes", où il est possible de rechercher des utilisateurs (**y compris les comptes sociaux/légers**, voir [Comptes pris en charge](../access.md#accounts-supported)).

Par défaut, la recherche ne renvoie que les comptes principaux et les egroups. Pour rechercher un compte de service, ajoutez « a : » et pour rechercher social/léger, ajoutez « l : ».

Les utilisateurs/groupes peuvent avoir les permissions `Viewer` ou `Editor`. Pour supprimer un partage, cliquez sur le bouton :material-dots-vertical: à côté de la personne/egroup puis sur `Remove share`.

![Partager un seul fichier](../../assets/screenshots/sharing.png)

Pour avoir un aperçu de tout ce qui est partagé avec d'autres, cliquez simplement sur "Partagé avec d'autres" dans le menu de gauche.

Après avoir défini les permissions (partage avec quelqu'un), il est également possible de copier l'url du fichier/dossier et de la donner à l'utilisateur destinataire (voir [URL universelles](../access.md#universal-urls)).