# Authenticated shares

As with the previous system, it's possible to share folders with other authenticated users. But now, **it's also possible to share single files** in both read and write modes!

To share a folder/directory, just right click it and click in `Share`. This will open the right sidebar, on the `People` option, where it's possible to search for users (**including social/lightweight accounts**, see [Accounts supported](../access.md#accounts-supported)).

By default, the search only returns the main accounts and egroups. To search for a service/secondary account, prepend `a:` and to search for social/lightweight, prepend `l:`.

Users/groups can have `Viewer` of `Editor` permissions. To remove a share, click in the :material-dots-vertical: button next to the person/egroup and then on `Remove share`.

![Sharing a single file](../../assets/screenshots/sharing.png)

To have an overview of everything that is being shared with others, just click in `Shares` - `Shared with others` on the left menu.

After setting the permissions (sharing with someone), it's also possible to copy the url of the file/folder and give it to the receiving user (see [Universal URLs](../access.md#universal-urls)).