# Partage et autorisations

Comme avec le système précédent, il est possible de partager des dossiers avec d'autres utilisateurs authentifiés. Mais maintenant, **il est également possible de partager des fichiers uniques** en mode lecture et écriture !

Pour partager un dossier/répertoire, faites un clic droit dessus et cliquez sur "Partager". Cela ouvrira la barre latérale droite avec des options pour créer un partage authentifié ou public.

Apprendre, comment créer

* [partages authentifiés](auth-share.md)
* [partages publiques](public-share.md)
* [Autorisations de refus dans les projets](../projects/manage_project_space.md#sharing-the-resources-inside-the-project)

<iframe width="576" height="360" frameborder="0" src="https://weblecture-player.web.cern.ch/?year=2023&amp;id=1275596" ?showtitle="true" allowfullscreen></iframe>
[Tutoriel: CERNBox Partage et autorisations sur twiki.cern.ch](https://twiki.cern.ch/twiki/bin/view/Edutech/CERNBoxShareAndAuthShare){:target="_blank"}



## Héritage des autorisations

Les partages/liens publics ajoutés à tous les dossiers parents seront affichés dans les vues "Personnes" et "Liens". Cela permet de mieux comprendre pourquoi quelqu'un a déjà accès à un fichier/dossier.

Dans la vue "Personnes", les utilisateurs autorisés sur le dossier parent seront visibles sans l'option de modifier leur rôle (lecteur/éditeur) ni l'option de supprimer le partage.

Dans la vue `Liens`, tous les liens seront visibles, avec la mention qu'il vient `Via <nom du dossier>`.

![Fichier déjà accessible via un lien parent](../../assets/screenshots/shared-via.png)
