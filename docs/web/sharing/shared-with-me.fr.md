# Afficher les partages internes

Si le partageur sélectionne l'option pour vous informer du partage, vous recevrez l'e-mail avec le lien vers ce partage.

La page "Partagé avec moi" affichera tout ce qui a été partagé avec le compte connecté. Dans l'aperçu du tableau, vous pouvez voir qui a partagé avec vous ("Partagé par"). Vous pouvez également voir si le partage a été partagé avec vous ou un e-groupe ("Partagé avec").

Vous pouvez masquer les partages, ils disparaîtront de la liste des partages. Les partages cachés ne sont pas supprimés, vous pouvez toujours cliquer sur "Afficher les partages cachés" pour les voir et les afficher plus tard.


![Partagé avec moi](../../assets/screenshots/shares-overview.png)


Pour améliorer la convivialité, il est désormais possible de sélectionner plusieurs partages et de les accepter/rejeter par lots. Il est également possible de regrouper les partages selon différents critères (propriétaire, temps, etc).

Pour accéder au contenu à l'intérieur d'un partage, cliquez simplement sur n'importe quel partage accepté (non accepté ou rejeté ne le permet pas).