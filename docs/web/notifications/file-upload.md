# File upload

You can also activate notifications when files are uploaded to a public folder you
own.

!!! info Note
    Keep in mind that this option is only available for public links with **uploader**
    permissions.

For this, you only need to click on: "Notify uploads" in a public link. After that,
any time somebody uploads a file or a folder using that link, you will get an email
with details about the action.

![Enabling notification for file uploads](../../assets/screenshots/notify-uploads.png)


## Notify a third party

It is possible to forward the notification to another email address if you want
someone else to receive the same email when a file is uploaded. Only one extra
address can be added, so if you want to notify more than one person, you can use
an e-group.

You will find the option for this in the three-dot menu at the right of a public
link.

![Enabling notification when creating a share](../../assets/screenshots/notify-third-party.png)
