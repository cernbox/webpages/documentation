# New share

You make CERNBox automatically send an email to a user or e-group when you share
a file or folder with them.

For this, you only need to tick the 'Notify via mail' when you create the share.

![Enabling notification when creating a share](../../assets/screenshots/enable-share-notification.png)

The user or e-group with whom you share will get an email right away.

If you forget to check the box, or want to remind them, you can always resend the
notification by opening the share menu and clicking in "Notify via mail" inside.

![Sending a notification for an already existing share](../../assets/screenshots/share-reminder.png)
