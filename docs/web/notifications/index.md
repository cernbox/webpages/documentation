# Introduction

CERNBox features real-time email notifications for various actions, such as receiving
a shared file or folder or uploading to a public folder you own.

With them, you can stay up-to-date about important activities happening in your
storage without constantly checking for updates manually.

!!! info Note
    Please note that the notification system is currently in **beta** stage. To
    enrol in the trial, [send an email to the CERNBox admins](mailto:cernbox-admins@cern.ch).
    Your participation and feedback are vital in helping us refine and improve its
    functionality and usability, and we are grateful for them!

## Activating notifications

As this feature is under active development, there are a limited set of notifications
available. We will keep adding new ones in the future.

- [New share](new-share.md)
- [File upload](file-upload.md)
