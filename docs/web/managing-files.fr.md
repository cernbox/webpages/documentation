# Gestion des fichiers


## Modes d'affichage des ressources

Vous pouvez modifier les modes d'affichage de vos ressources dans le coin supérieur droit de la zone des fichiers.

### Vue de tableau

La vue tableau répertorie vos fichiers dans un tableau. Vous pouvez trier les fichiers en cliquant sur les noms des colonnes correspondantes.
![Vue tableau](../assets/screenshots/table_view.png)

### Affichage de tableau condensé
La vue de tableau condensé affiche les fichiers de manière plus compacte afin que vous puissiez voir plus de fichiers à la fois.

### Affichage des tuiles
Avec la vue mosaïques, les fichiers sont affichés sous forme de mosaïques. Vous pouvez trier les fichiers en sélectionnant une option dans "Trier par". Il est également possible de modifier la taille des tuiles dans les paramètres d'affichage.
![Vue mosaïques avec paramètres](../assets/screenshots/tiles_view2.png)

## Ajout de fichiers

Pour ajouter des fichiers, vous pouvez utiliser les boutons `Télécharger` ou `Nouveau`. Pour voir quelles extensions de fichiers sont visibles/modifiables par les applications intégrées, veuillez consulter la page [applications](apps/index.md#online-viewingediting-capabilities).

![Créer un nouveau fichier](../assets/screenshots/new-file-menu.png)

Il est également possible de glisser-déposer un fichier/dossier au centre de la page.

## Organisme

Pour déplacer des fichiers, il est possible de glisser-déposer des fichiers dans des sous-répertoires, comme auparavant, mais il existe désormais également les options "Déplacer" et "Copier".

Les actions sur les fichiers sont accessibles via les menus du clic droit ou les menus :material-dots-vertical:. Les fichiers sélectionnés auront des options supplémentaires dans la barre latérale droite, ouvertes en cliquant sur "Détails" ou sur le bouton de la barre latérale (:octicons-sidebar-expand-16:).

Pour afficher ou masquer les fichiers cachés (fichiers dont le nom commence par un point `.`), veuillez définir l'option dans le menu des paramètres d'affichage des fichiers (:material-cog:).

## Favoris

Il est désormais possible de mettre en favori de manière fiable un fichier/dossier depuis n'importe quel endroit, en créant un raccourci disponible à partir de la page "Favoris".
