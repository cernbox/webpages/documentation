# Bienvenue dans la nouvelle version de CERNBox !

Ce document vise à expliquer les fonctionnalités de base du nouveau et repensée [CERNBox](https://new.cernbox.cern.ch){target=_blank}, ainsi qu'à présenter les toutes nouvelles fonctionnalités qui lui sont exclusives.

## Qu'est-ce qui a changé ?

Tout ce qui était disponible dans le système précédent est également disponible avec un nouveau look moderne dans le nouveau, y compris les 1 To d'espaces de stockage et les assurances de sécurité et de confidentialité fournies par le centre de données du CERN.

La nouvelle interface vise à améliorer la convivialité. Voici quelques modifications d'interface que nous avons apportées :

 * [Changements d'interface](interface-changes.md)

## Nouvelles fonctionnalités

Nous vous invitons à lire les sujets présentés dans le menu de gauche.
Celles-ci incluent des explications sur les nouvelles fonctionnalités majeures introduites :

* Interface utilisateur plus moderne et intuitive
* [URL universelles](access.md#universal-urls)
* [Partage de fichier unique](sharing/index.md)
* [Écosystème d'applications amélioré](apps/index.md)
* [Intégration améliorée des projets](projects/index.md)
* [Prise en charge des comptes légers](access.md#accounts-supported)
* [Refus d'accès](projects/manage_project_space.md#sharing-the-resources-inside-the-project)




## Saviez-vous que vous pouviez travailler avec des fichiers Markdown et Office ?


 
* [Comment utiliser Markdown](apps/codimd.md)
* [Comment utiliser Office](apps/office_online.md)


!!! Info "D'autres fonctionnalités arriveront dans les mois à venir" 

    * Intégrations avec d'autres services (Indico déjà disponible)
    * Recherche améliorée
    * Audit et reporting/notifications
    * Interface utilisateur de restauration de sauvegarde
    * Intégration améliorée du client de synchronisation

  

## Configuration requise pour le bureau

Lorsque vous utilisez un appareil de bureau tel qu'un PC, un ordinateur portable, y compris Microsoft Surface, les navigateurs suivants sont pris en charge :

| Browser | Version supportée | Système opérateur             |
|---------|-------------------|-------------------------------|
| Chrome  | Les 2 dernières versions | Windows 10+ ,   macOS 12.5+ ,  Linux |
| Firefox | Les 2 dernières versions | Windows 10+ ,  macOS 12.5+ ,  Linux |
| Edge    | Les 2 dernières versions | Windows 10+                   |
| Safari  | 16+               | macOS 12.6+                   |

Vous pouvez trouver les versions prises en charge [ici.](https://browserslist.dev/?q=bGFzdCAyIHZlcnNpb25z){target=_blank} 

