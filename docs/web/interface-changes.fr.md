# L'interface


## Mode clair et sombre

Sur le côté droit de la barre supérieure, vous pouvez maintenant activer le nouveau mode sombre pour l'interface.

![Mode lumière](../assets/screenshots/light-mode.png)
![Mode sombre](../assets/screenshots/dark-mode.png)

## Interactions avec la liste des fichiers


Le menu contextuel est désormais également accessible avec le clic droit en plus du menu :material-dots-vertical:.
Cela permet aux utilisateurs d'effectuer des actions typiques encore plus rapidement, par exemple, télécharger des documents ou ouvrir des documents dans les applications.

[En savoir plus sur la gestion des fichiers](managing-files.md)

## Partage de documents simplifié

La nouvelle interface utilisateur Web facilite le partage de contenu. L'action rapide "Copier le lien rapide" vous permet de copier le lien public de la ressource appelé "Lien rapide". C'est par défaut le premier des liens publics.

Il est tout aussi simple d'inviter des utilisateurs que des lecteurs ou des éditeurs à consulter ou à collaborer sur des documents en cliquant sur l'action rapide "Partager". De plus, vous pouvez envoyer une notification par e-mail aux partages sélectionnés en cliquant sur "Partager" en cochant la case correspondante. Vous pouvez notifier rétroactivement un partage existant en sélectionnant l'option dans le menu des options du partage correspondant.

![Partage](../assets/screenshots/sharing2.png)

## Aperçu des actions

Nous avons amélioré l'aperçu de vos partages. Vous pouvez appliquer différents paramètres de regroupement de la liste des partages pour une exploration plus facile. Dans la section "Partagé avec moi", vous pouvez masquer les partages et les afficher dans une liste de partages masqués.
![Aperçu des partages](../assets/screenshots/shares-overview.png)

[En savoir plus sur le partage](sharing/index.md)

## Guide "Nouvelles fonctionnalités"

Obtenez un aperçu des nouvelles fonctionnalités en cliquant sur "Nouvelles fonctionnalités" dans la barre supérieure.


## Accessibilité améliorée

Les nouvelles fonctionnalités d'accessibilité ont été ajoutées à l'interface utilisateur. La nouvelle interface utilisateur Web peut être contrôlée via le clavier, offre un contraste de couleur amélioré pour une meilleure lisibilité et prend en charge la navigation et le fonctionnement via un lecteur d'écran.

